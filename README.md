# Web Service

框架：SeviceStack.Net

使用版本：v4.5.0

免费配额：

Whilst ServiceStack v4 is a commercially-supported product, we also allow free usage for small projects and evaluation purposes. 
The NuGet packages above include the quota's below which can be unlocked with a license key:

- **10 Operations in ServiceStack** (i.e. Request DTOs)
- **10 Database Tables in OrmLite**
- **10 DynamoDB Tables in PocoDynamo**
- **20 Different Types in Redis Client Typed APIs**
- **6000 requests per hour with the Redis Client**

*已破解* 





