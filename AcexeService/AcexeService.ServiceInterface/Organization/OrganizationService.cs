﻿using AcexeService.Models;
using AcexeService.ServiceModel.Organization;
using ServiceStack;
using ServiceStack.OrmLite;
using System.Collections.Generic;

namespace AcexeService.ServiceInterface.Organization
{
    [Authenticate]
    public class OrganizationService : Service
    {
        /// <summary>
        /// 取单个实体
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(GetOrganization request)
        {
            string id = request.OrgId;
            var list = Db.Select<ORG_ORGANIZATION>(c => c.ORGID == id);
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }
        /// <summary>
        /// 获取组织列表不分页
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(GetOrganizationList request)
        {
            var q = Db.From<ORG_ORGANIZATION>()
           .LeftJoin<ORG_ORGANIZATION, SYS_DICTIONARY_DETAIL>((a, b) => (a.ORGCATE == b.DETAILID && b.DICID == DictConfig.ORGANIZATION_CATEGORY_DICID))
           .Where(x => x.ISDELETED == false)
           .OrderBy(x => x.ORDERNUM);

            if (!string.IsNullOrEmpty(request.CateId))
            {
                q.And(x => x.ORGCATE == request.CateId);
            }

            q.Select<ORG_ORGANIZATION, SYS_DICTIONARY_DETAIL>((a, b) => new
            {
                a,
                CATEGROY = b.DETAILNAME
            });

            var list = Db.Select<Dictionary<string, object>>(q);
            return list;
        }
        /// <summary>
        /// 列表（分页、导出）
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetOrganizationPageList request)
        {
            var miniuigridmodel = new MiniUiGridModel<StrObjDict>();
            var model = request.ORGANIZATIONModel;

            var q = Db.From<ORG_ORGANIZATION>()
           .LeftJoin<ORG_ORGANIZATION, SYS_DICTIONARY_DETAIL>((a, b) => (a.ORGCATE == b.DETAILID && b.DICID == DictConfig.ORGANIZATION_CATEGORY_DICID))
           .Where(x => x.ISDELETED == false);

            if (!string.IsNullOrEmpty(model.ORGCATE))
            {
                q.And(x => x.ORGCATE == model.ORGCATE);
            }

            q.And(x => x.ISDELETED == model.ISDELETED);

            if (!string.IsNullOrEmpty(model.ORGNAME))
                q.And(x => x.ORGNAME.Contains(model.ORGNAME));


            miniuigridmodel.total = Db.Scalar<int>(q.Select(Sql.Count("*")));
            if (!request.Page.IsGetWholeData)
            {
                q.Take(request.Page.PageSize);
                q.Skip(request.Page.PageSize * request.Page.PageIndex);
            }

            if (!string.IsNullOrEmpty(request.Page.SortField))
            {
                if (request.Page.SortOrder == "asc")
                    q.OrderByFields(request.Page.SortField);
                else
                    q.OrderByFieldsDescending(request.Page.SortField);
            }
            else
            {
                q.OrderBy(x => x.ORGNO);
            }

            q.Select<ORG_ORGANIZATION, SYS_DICTIONARY_DETAIL>((a, b) => new
            {
                a,
                CATEGROY = b.DETAILNAME
            });

            // var list = Db.Select<ORG_ORGANIZATION>(q.Select(c => c));
            var list = Db.Select<Dictionary<string, object>>(q);
            //return list;

            miniuigridmodel.data = list.ToListStrObjDict();

            return miniuigridmodel;
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(AddOrganization request)
        {
            var dto = new MessageDto();
            var model = request.SysOrganizationModel;
            if (string.IsNullOrEmpty(model.ORGID))
            {
                dto.Flag = false;
                dto.Msg = "组织ID不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORGNO))
            {
                dto.Flag = false;
                dto.Msg = "组织编号不不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORGNAME))
            {
                dto.Flag = false;
                dto.Msg = "组织名称不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORGPID))
            {
                dto.Flag = false;
                dto.Msg = "上级组织名称不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORDERNUM.ToString()))
            {
                dto.Flag = false;
                dto.Msg = "排序号不能为空";
                return dto;
            }
            try
            {
                Db.Insert<ORG_ORGANIZATION>(model);
                dto.Flag = true;
                dto.Msg = "新增成功";
            }
            catch (System.Exception ex)
            {

                dto.Flag = false;
                dto.Msg = ex.Message.ToString();
            }
            return dto;
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(EditOrganization request)
        {
            var dto = new MessageDto();
            var model = request.SysOrganizationModel;
            if (string.IsNullOrEmpty(model.ORGID))
            {
                dto.Flag = false;
                dto.Msg = "组织ID不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORGNO))
            {
                dto.Flag = false;
                dto.Msg = "组织编号不不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORGNAME))
            {
                dto.Flag = false;
                dto.Msg = "组织名称不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORGPID))
            {
                dto.Flag = false;
                dto.Msg = "上级组织名称不能为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.ORDERNUM.ToString()))
            {
                dto.Flag = false;
                dto.Msg = "排序号不能为空";
                return dto;
            }
            try
            {
                var list = Db.Select<ORG_ORGANIZATION>(c => c.ORGID == model.ORGID);
                if (list.Count == 0)
                {
                    dto.Flag = false;
                    dto.Msg = "您编辑的组织不存在，请刷新页面后重试";
                    return dto;
                }

                var editmodel = list[0];
                editmodel = AcexeService.ServiceInterface.Lib.AutoTransfer.GetModel<ORG_ORGANIZATION>(editmodel, model);
                Db.Update<ORG_ORGANIZATION>(editmodel);
                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (System.Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        /// <summary>
        /// 删除(批量)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(DeleteOrganization request)
        {
            var dto = new MessageDto();

            if (request.OrganizationId.Count == 0)
            {
                dto.Flag = false;
                dto.Msg = "组织ID为空";
                return dto;
            }
            try
            {
                //只作虚删除
                var orgids = request.OrganizationId.ToSqlString();
                Db.ExecuteSql("UPDATE ORG_ORGANIZATION SET ISDELETED = @isdeleted WHERE ORGID IN(" + orgids + ")", new { isdeleted = 1 });
                dto.Flag = true;
                dto.Msg = "删除成功";
                return dto;
            }
            catch (System.Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }
    }
}
