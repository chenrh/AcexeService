﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.ServiceInterface.Lib
{
    public class AutoTransfer
    {
        public static T GetModel<T>(T DbModel, T FormModel)
        {
            System.Reflection.PropertyInfo[] prop = typeof(T).GetProperties();
            for (int i = 0; i < prop.Length; i++)
            {
                System.Reflection.PropertyInfo pi = prop[i];
                object dbvalue = pi.GetValue(DbModel);
                object formvalue = pi.GetValue(FormModel);
                if (formvalue != null)
                {
                    pi.SetValue(DbModel, formvalue);
                }
            }
            return DbModel;
        }
    }
}
