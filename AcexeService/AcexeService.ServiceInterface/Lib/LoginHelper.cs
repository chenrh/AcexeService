﻿using AcexeService.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using System;

namespace AcexeService.ServiceInterface.Lib
{

    public class LoginModel
    {
        public SYS_USER LoginForm { get; set; }

        public string LoginIp { get; set; }

        public string HttpHeader { get; set; }
    }

    public class LoginHelper : Service
    {

        public MessageDto CheckLogin(LoginModel request)
        {
            var dto = new MessageDto();
            var log = new SYS_LOG_LOGIN()
            {
                LOGINON = DateTime.Now,
                LOGID = Guid.NewGuid().ToString(),
                LOGINIP = request.LoginIp,
                LOGINREMARK = request.HttpHeader,
                LOGINACCOUNT = request.LoginForm.USERACCOUNT,
                LOGINMESSAGE = "",
                UESRID = "LOGIN FAILED",
                LOGINSUCCESS = false
            };

            string useraccount = request.LoginForm.USERACCOUNT;
            string userpwd = request.LoginForm.USERPWD;


            var list = Db.Select<SYS_USER>(x => x.USERACCOUNT == useraccount);
            if (list.Count == 0)
            {
                dto.Flag = false;
                dto.Msg = "帐号不存在";

                log.LOGINSUCCESS = dto.Flag;
                log.LOGINMESSAGE = dto.Msg;
                Db.Insert<SYS_LOG_LOGIN>(log);
                return dto;
            }

            var model = list[0];
            model.USERPWD = model.USERPWD ?? "";
            if (model.USERPWD.ToUpper() != userpwd.ToUpper())
            {
                dto.Flag = false;
                dto.Msg = "帐号密码不匹配";

                log.LOGINSUCCESS = dto.Flag;
                log.LOGINMESSAGE = dto.Msg;
                Db.Insert<SYS_LOG_LOGIN>(log);
                return dto;
            }

            if (model.ISVALID == false)
            {
                dto.Flag = false;
                dto.Msg = "帐号已禁用";

                log.LOGINSUCCESS = dto.Flag;
                log.LOGINMESSAGE = dto.Msg;
                Db.Insert<SYS_LOG_LOGIN>(log);
                return dto;
            }

            if (model.ISDELETED == true)
            {
                dto.Flag = false;
                dto.Msg = "帐号已作废";

                log.LOGINSUCCESS = dto.Flag;
                log.LOGINMESSAGE = dto.Msg;
                Db.Insert<SYS_LOG_LOGIN>(log);
                return dto;
            }

            dto.Flag = true;
            dto.Msg = "登录成功";

            dto.Data = model;

            // 客户端每次请求接口都会走这里，过滤不必要存储的日志
            // 客户端由每次的 用户名密码验证，改为了session 方式验证
            //if (!log.LOGINREMARK.Contains("ServiceStack .NET Client"))
            //{
            log.UESRID = model.USERID;
            log.LOGINSUCCESS = dto.Flag;
            log.LOGINMESSAGE = dto.Msg;
            Db.Insert<SYS_LOG_LOGIN>(log);

            // 更新用户表登录相关字段数据
            model.LOGINIP = log.LOGINIP;
            model.LOGININFO = log.LOGINREMARK;
            model.LOGINON = log.LOGINON;
            Db.Update<SYS_USER>(model);
            //}

            return dto;
        }
    }
}
