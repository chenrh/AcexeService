﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService
{
    public static class Extends
    {
        public static string ToSqlString(this List<string> list)
        {
            string result = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (i != 0)
                {
                    result += string.Format(",'{0}'", list[i]);
                }
                else
                {
                    result = string.Format("'{0}'", list[i]);
                }
            }
            return result;
        }

        public static List<AcexeService.Models.StrObjDict> ToListStrObjDict(this List<Dictionary<string, object>> list)
        {
            var rets = new List<AcexeService.Models.StrObjDict>();
            for (int i = 0; i < list.Count; i++)
            {
                Models.StrObjDict temp = new Models.StrObjDict();
                foreach (KeyValuePair<string, object> kvp in list[i])
                {
                    temp.Add(kvp.Key, kvp.Value);
                }
                rets.Add(temp);
            }
            return rets;
        }
    }
}
