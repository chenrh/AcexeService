﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.ServiceInterface.Lib
{
    public class SqlStatement
    {
        /// <summary>
        /// 当使用实体类做查询条件时，可以本方法转化为条件语句
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string GetWhereStatement<T>(T model, string alias = "")
        {
            if (!string.IsNullOrEmpty(alias))
            {
                alias = alias.Trim();
                alias = alias + ".";
            }

            StringBuilder sb = new StringBuilder();
            System.Reflection.PropertyInfo[] prop = typeof(T).GetProperties();
            for (int i = 0; i < prop.Length; i++)
            {
                System.Reflection.PropertyInfo pi = prop[i];
                object value = pi.GetValue(model);
                if (value != null)
                {
                    sb.AppendFormat(" AND {0}{1} = '{2}'", alias, pi.Name, value);
                }
            }

            return sb.ToString();
        }

        public static void GetOrderBySqlExpress<T>(ref ServiceStack.OrmLite.SqlExpression<T> q, AcexeService.Models.PageParam Page)
        {
            // 现在的问题： 字段名含有关键字，导致用排序字符串时报错
            // 例如: Potential illegal fragment detected: UPDATEON
            // ,CREATEBY,ISDELETE 等字段做排序统统报错

          

        }
    }
}
