﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;
using AcexeService.ServiceModel;
using ServiceStack.Auth;
using ServiceStack.Logging;

namespace AcexeService.ServiceInterface
{
    [Authenticate]
    public class MyServices : Service
    {
        public object Any(Hello request)
        {

            var logger = LogManager.GetLogger(typeof(MyServices));

            logger.Info("消息");
            logger.Warn("警告");
            logger.Error("异常");
            logger.Fatal("错误");


            return new HelloResponse { Result = "Hello, {0}!".Fmt(request.Name) };
        }

        [Route("/reset")]
        public class Reset { }
        public object Any(Reset request)
        {
            ResetUsers((OrmLiteAuthRepository)TryResolve<IAuthRepository>());
            return "OK";
        }

        public static void ResetUsers(OrmLiteAuthRepository authRepo)
        {
            //
            //OrmLiteAuthRepository Db tables have not been initialized. Try calling 'InitSchema()' in your AppHost Configure method.
            //authRepo.DropAndReCreateTables();
        }


    }
}