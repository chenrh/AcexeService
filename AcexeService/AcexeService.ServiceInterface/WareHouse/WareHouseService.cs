﻿//
// 陈日红@2017/02/17 16:20
//
// 仓库模块 服务接口

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcexeService.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using AcexeService.ServiceModel.WareHouse;

namespace AcexeService.ServiceInterface
{
    [Authenticate]
    public class WareHouseService : Service
    {
        /// <summary>
        /// 取单个实体
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(GetWareHouse request)
        {
            string id = request.WareId;
            var list = Db.Select<WARE_WAREHOUSE>(c => c.WAREID == id);
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        /// <summary>
        /// 列表（分页、导出）
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetWareHouseList request)
        {
            var miniuigridmodel = new MiniUiGridModel<WARE_WAREHOUSE>();
            var model = request.WareHouseModel;

            var q = Db.From<WARE_WAREHOUSE>();

            if (!string.IsNullOrEmpty(request.FuzzyWareName))
            {
                q.And(p => p.WARENAME.Contains(request.FuzzyWareName));
            }

            q.And(x => x.ISDELETED == model.ISDELETED);

            if (!string.IsNullOrEmpty(model.WARENAME))
                q.And(x => x.WARENAME.Contains(model.WARENAME));


            miniuigridmodel.total = Db.Scalar<int>(q.Select(Sql.Count("*")));
            if (!request.Page.IsGetWholeData)
            {
                q.Take(request.Page.PageSize);
                q.Skip(request.Page.PageSize * request.Page.PageIndex);
            }

            if (!string.IsNullOrEmpty(request.Page.SortField))
            {
                if (request.Page.SortOrder == "asc")
                    q.OrderByFields(request.Page.SortField);
                else
                    q.OrderByFieldsDescending(request.Page.SortField);
            }
            else
            {
                q.OrderBy(x => x.WARENO);
            }

            var list = Db.Select<WARE_WAREHOUSE>(q.Select(c => c));
            miniuigridmodel.data = list;

            return miniuigridmodel;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(AddWareHouse request)
        {
            var dto = new MessageDto();
            var model = request.WareHouseModel;

            if (string.IsNullOrEmpty(model.WAREID))
            {
                dto.Flag = false;
                dto.Msg = "仓库ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.WARENAME))
            {
                dto.Flag = false;
                dto.Msg = "仓库名称不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.WARENO))
            {
                dto.Flag = false;
                dto.Msg = "仓库编号不允许为空";
                return dto;
            }

            try
            {
                Db.Insert<WARE_WAREHOUSE>(model);
                dto.Flag = true;
                dto.Msg = "新增成功";
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(EditWareHouse request)
        {
            var dto = new MessageDto();
            var model = request.WareHouseModel;

            if (string.IsNullOrEmpty(model.WAREID))
            {
                dto.Flag = false;
                dto.Msg = "仓库ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.WARENAME))
            {
                dto.Flag = false;
                dto.Msg = "仓库名称不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.WARENO))
            {
                dto.Flag = false;
                dto.Msg = "仓库编号不允许为空";
                return dto;
            }

            try
            {
                var list = Db.Select<WARE_WAREHOUSE>(c => c.WAREID == model.WAREID);
                if (list.Count == 0)
                {
                    dto.Flag = false;
                    dto.Msg = "您编辑的仓库不存在，请刷新页面后重试";
                    return dto;
                }

                var editmodel = list[0];
                editmodel = AcexeService.ServiceInterface.Lib.AutoTransfer.GetModel<WARE_WAREHOUSE>(editmodel, model);
                Db.Update<WARE_WAREHOUSE>(editmodel);
                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        /// <summary>
        /// 删除(单个)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(DeleteWareHouse request)
        {
            var dto = new MessageDto();

            if (string.IsNullOrEmpty(request.WareId))
            {
                dto.Flag = false;
                dto.Msg = "参数仓库id为空";
                return dto;
            }
            try
            {
                Db.UpdateOnly(new WARE_WAREHOUSE { ISDELETED = true, UPDATEBY = request.UpdateBy, UPDATEON = DateTime.Now }
                    , where: p => p.WAREID == request.WareId
                    , onlyFields: p => new { p.ISDELETED, p.UPDATEON, p.UPDATEBY });

                dto.Flag = true;
                dto.Msg = "作废成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }

        /// <summary>
        /// 删除(批量)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(DeleteWareHouses request)
        {
            var dto = new MessageDto();

            if (request.ListWareId.Count == 0)
            {
                dto.Flag = false;
                dto.Msg = "参数仓库id为空";
                return dto;
            }
            try
            {
                var q = Db.From<WARE_WAREHOUSE>().Where(x => Sql.In(x.WAREID, request.ListWareId));
                Db.UpdateOnly(new WARE_WAREHOUSE { ISDELETED = true, UPDATEBY = request.UpdateBy, UPDATEON = DateTime.Now }
                    , q);

                dto.Flag = true;
                dto.Msg = "作废成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }


        public object Any(GetTreeGridWareHouse request)
        {
            var q = Db.From<WARE_WAREHOUSE>();
            q.And(x => x.ISDELETED == false);
            q.OrderBy(x => x.WARENO);
            var list = Db.Select<WARE_WAREHOUSE>(q.Select(c => new { c.WAREID, c.WARENAME, c.WARENO }));
            return list;
        }
    }
}
