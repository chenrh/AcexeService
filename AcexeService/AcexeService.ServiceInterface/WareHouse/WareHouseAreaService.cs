﻿//
// 陈日红@2017/02/17 16:20
//
// 仓库库区模块 服务接口

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcexeService.Models;
using ServiceStack;
using ServiceStack.OrmLite;
using AcexeService.ServiceModel.WareHouse;

namespace AcexeService.ServiceInterface
{
    [Authenticate]
    public class WareHouseAreaService : Service
    {
        /// <summary>
        /// 取单个实体
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(GetWareHouseArea request)
        {
            string id = request.AreaId;
            var list = Db.Select<WARE_WAREHOUSE_AREA>(c => c.AREAID == id);
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        /// <summary>
        /// 列表（分页、导出）
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetWareHouseAreaList request)
        {
            var miniuigridmodel = new MiniUiGridModel<WARE_WAREHOUSE_AREA>();
            var model = request.WareHouseAreaModel;

            var q = Db.From<WARE_WAREHOUSE_AREA>().Where(x => x.WAREID == model.WAREID);

            q.And(x => x.ISDELETED == model.ISDELETED);
            q.And(x => x.ISDELETED == false);

            miniuigridmodel.total = Db.Scalar<int>(q.Select(Sql.Count("*")));
            if (!request.Page.IsGetWholeData)
            {
                q.Take(request.Page.PageSize);
                q.Skip(request.Page.PageSize * request.Page.PageIndex);
            }

            if (!string.IsNullOrEmpty(request.Page.SortField))
            {
                if (request.Page.SortOrder == "asc")
                    q.OrderByFields(request.Page.SortField);
                else
                    q.OrderByFieldsDescending(request.Page.SortField);
            }
            else
            {
                q.OrderBy(x => x.AREANO);
            }

            var list = Db.Select<WARE_WAREHOUSE_AREA>(q.Select(c => c));
            miniuigridmodel.data = list;

            return miniuigridmodel;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(AddWareHouseArea request)
        {
            var dto = new MessageDto();
            var model = request.WareHouseAreaModel;

            if (string.IsNullOrEmpty(model.AREAID))
            {
                dto.Flag = false;
                dto.Msg = "仓库ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.AREANO))
            {
                dto.Flag = false;
                dto.Msg = "库区编号不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.WARENO))
            {
                dto.Flag = false;
                dto.Msg = "仓库编号不允许为空";
                return dto;
            }

            try
            {
                Db.Insert<WARE_WAREHOUSE_AREA>(model);
                dto.Flag = true;
                dto.Msg = "新增成功";
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(EditWareHouseArea request)
        {
            var dto = new MessageDto();
            var model = request.WareHouseAreaModel;

            if (string.IsNullOrEmpty(model.AREAID))
            {
                dto.Flag = false;
                dto.Msg = "仓库ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.AREANO))
            {
                dto.Flag = false;
                dto.Msg = "库区编号不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.WARENO))
            {
                dto.Flag = false;
                dto.Msg = "仓库编号不允许为空";
                return dto;
            }

            try
            {
                var list = Db.Select<WARE_WAREHOUSE_AREA>(c => c.AREAID == model.AREAID);
                if (list.Count == 0)
                {
                    dto.Flag = false;
                    dto.Msg = "您编辑的仓库不存在，请刷新页面后重试";
                    return dto;
                }

                var editmodel = list[0];
                editmodel = AcexeService.ServiceInterface.Lib.AutoTransfer.GetModel<WARE_WAREHOUSE_AREA>(editmodel, model);
                Db.Update<WARE_WAREHOUSE_AREA>(editmodel,p=>p.AREAID==editmodel.AREAID);
                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        /// <summary>
        /// 删除(单个)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(DeleteWareHouseArea request)
        {
            var dto = new MessageDto();

            if (string.IsNullOrEmpty(request.WareAreaId))
            {
                dto.Flag = false;
                dto.Msg = "参数库区id为空";
                return dto;
            }
            try
            {
                Db.UpdateOnly(new WARE_WAREHOUSE_AREA { ISDELETED = true, UPDATEBY = request.UpdateBy, UPDATEON = DateTime.Now }
                    , where: p => p.AREAID == request.WareAreaId
                    , onlyFields: p => new { p.ISDELETED, p.UPDATEON, p.UPDATEBY });

                dto.Flag = true;
                dto.Msg = "作废成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }

        /// <summary>
        /// 删除(批量)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(DeleteWareHouseAreas request)
        {
            var dto = new MessageDto();

            if (request.ListWareAreaId.Count == 0)
            {
                dto.Flag = false;
                dto.Msg = "参数库区id为空";
                return dto;
            }
            try
            {
                var q = Db.From<WARE_WAREHOUSE_AREA>().Where(x => Sql.In(x.AREAID, request.ListWareAreaId));
                Db.UpdateOnly(new WARE_WAREHOUSE_AREA { ISDELETED = true, UPDATEBY = request.UpdateBy, UPDATEON = DateTime.Now }
                    , q);

                dto.Flag = true;
                dto.Msg = "作废成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }

    }
}
