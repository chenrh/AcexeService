﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Configuration;
using AcexeService.Models;

namespace AcexeService.ServiceInterface
{
    public class CustomUserSession : AuthUserSession
    {

        [DataMember]
        public AcexeService.Models.SYS_USER LoginSession { get; set; }

        public override void OnAuthenticated(IServiceBase authService, IAuthSession session,
            IAuthTokens tokens, Dictionary<string, string> authInfo)
        {
            var authinfo = authService.GetSessionBag().Get<AcexeService.Models.SYS_USER>();
            if (authinfo != null)
            {
                this.LoginSession = authinfo;
                session.DisplayName = authinfo.USERNAME;
                session.Email = authinfo.USEREMAIL;
                session.IsAuthenticated = true;
                session.UserName = authinfo.USERNAME;
                session.UserAuthName = authinfo.USERACCOUNT;
                session.UserAuthId = authinfo.USERID;
            }
        }
    }

    #region SessionService



    public class UnAuthInfo
    {
        public string CustomInfo { get; set; }
    }

    [Authenticate]
    [Route("/session")]
    public class GetSession : IReturn<GetSessionResponse>
    {
    }
    [Authenticate]
    [Route("/session/edit/{CustomName}")]
    public class UpdateSession : IReturn<GetSessionResponse>
    {
        public string CustomName { get; set; }
    }

    public class GetSessionResponse
    {
        public CustomUserSession Result { get; set; }

        public UnAuthInfo UnAuthInfo { get; set; }

        public ResponseStatus ResponseStatus { get; set; }
    }


    public class SessionService : Service
    {
        public object Any(GetSession request)
        {
            return new GetSessionResponse
            {
                Result = SessionAs<CustomUserSession>(),
                UnAuthInfo = SessionBag.Get<UnAuthInfo>(typeof(UnAuthInfo).Name),
            };
        }
    }
    #endregion
}