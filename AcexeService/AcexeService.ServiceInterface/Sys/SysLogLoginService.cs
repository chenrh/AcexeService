﻿//
// 登录日志数据服务
//
// 陈日红@2017/2/9 9:33

using AcexeService.Models;
using AcexeService.ServiceModel.Sys;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;

namespace AcexeService.ServiceInterface
{
    public class SysLogLoginService : Service
    {

        /// <summary>
        /// 人员列表取登录日志数据服务
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetLoginLog request)
        {
            string userid = request.UserId;
            int? size = request.Size;

            var q = Db.From<SYS_LOG_LOGIN>()
             .Join<SYS_LOG_LOGIN, SYS_USER>((a, b) => a.UESRID == b.USERID)
             .Select<SYS_LOG_LOGIN, SYS_USER>((e, d) => new { e, d.USERNAME });

            if (!string.IsNullOrEmpty(userid))
            {
                q.And(c => c.UESRID == userid);
            }
            if (size.HasValue)
            {
                q.Take(size.Value);
            }
            q.OrderByDescending(x => x.LOGINON);

            var dict = Db.Select<Dictionary<string, object>>(q);

            return dict;
        }
    }
}
