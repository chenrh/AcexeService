﻿using AcexeService.Models;
using AcexeService.ServiceModel.Sys;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;

namespace AcexeService.ServiceInterface
{

    [Authenticate]
    public class SysDictionaryService : Service
    {

        public object Any(GetDictionary request)
        {
            string DicId = request.DicId;
            var list = Db.Select<SYS_DICTIONARY>(c => c.DICID == DicId);
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        public object Any(GetDictionaryList request)
        {
            var pagresult = new MiniUiGridModel<SYS_DICTIONARY>();

            var q = Db.From<SYS_DICTIONARY>().Where(x => x.ISDELETED == request.SysDictionaryModel.ISDELETED);

            if (!string.IsNullOrEmpty(request.FuzzyDictionary))
            {
                q.And(x => x.DICNAME.Contains(request.FuzzyDictionary) || x.DICID.Contains(request.FuzzyDictionary));
            }

            // 数据总行数(需要在分页之前赋值)
            pagresult.total = Db.Scalar<int>(q.Select(Sql.Count("*")));

            if (!request.Page.IsGetWholeData)
            {
                q.Take(request.Page.PageSize);
                q.Skip(request.Page.PageSize * request.Page.PageIndex);
            }

            if (!string.IsNullOrEmpty(request.Page.SortField))
            {
                if (request.Page.SortOrder == "asc")
                    q.OrderByFields(request.Page.SortField);
                else
                    q.OrderByFieldsDescending(request.Page.SortField);
            }
            else
            {
                q.OrderBy(x => x.ORDERNUM);
            }

            var list = Db.Select<SYS_DICTIONARY>(q.Select(c => c));
            pagresult.data = list;

            return pagresult;
        }

        public object Any(AddDictionary request)
        {
            var dto = new MessageDto();
            var model = request.SysDictionaryModel;

            if (string.IsNullOrEmpty(model.DICID))
            {
                dto.Flag = false;
                dto.Msg = "字典ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.DICNAME))
            {
                dto.Flag = false;
                dto.Msg = "字典名称不允许为空";
                return dto;
            }

            try
            {
                Db.Insert<SYS_DICTIONARY>(model);
                dto.Flag = true;
                dto.Msg = "新增成功";
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        public object Any(EditDictionary request)
        {
            var dto = new MessageDto();
            var model = request.SysDictionaryModel;

            if (string.IsNullOrEmpty(model.DICID))
            {
                dto.Flag = false;
                dto.Msg = "字典ID不允许为空";
                return dto;
            }
            if (string.Equals(model.DICNAME, ""))
            {
                dto.Flag = false;
                dto.Msg = "字典名称不允许为空";
                return dto;
            }


            try
            {
                var list = Db.Select<SYS_DICTIONARY>(c => c.DICID == model.DICID);
                if (list.Count == 0)
                {
                    dto.Flag = false;
                    dto.Msg = "您编辑的字典不存在，请刷新页面后重试";
                    return dto;
                }

                var editmodel = list[0];
                editmodel = AcexeService.ServiceInterface.Lib.AutoTransfer.GetModel<SYS_DICTIONARY>(editmodel, model);
                Db.Update<SYS_DICTIONARY>(editmodel);
                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        public object Any(DeleteDictionary request)
        {
            var dto = new MessageDto();
            if (request.SysDictionaryId.Count == 0)
            {
                dto.Flag = false;
                dto.Msg = "空记录";
                return dto;
            }
            var id = request.SysDictionaryId.ToSqlString();
            try
            {
                Db.Update<SYS_DICTIONARY>();
                dto.Flag = true;
                dto.Msg = "删除成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
                throw;
            }
        }

        // 编辑字典 连带字典明细
        public object Any(EditDictionaryWithDetail request)
        {
            var model = request.SysDictionaryModel;
            var listadd = request.ListAdd;
            var listdel = request.ListDelete;
            var listedit = request.ListModify;

            var dto = (MessageDto)Any(new EditDictionary()
            {
                SysDictionaryModel = model
            });
            if (dto.Flag == false)
            {
                return dto;
            }

            try
            {

                using (System.Data.IDbTransaction dbTrans = Db.OpenTransaction())
                {
                    // 增加
                    for (int i = 0; i < listadd.Count; i++)
                    {
                        Db.Insert<SYS_DICTIONARY_DETAIL>(listadd[i]);
                    }

                    // 删除
                    for (int i = 0; i < listdel.Count; i++)
                    {
                        Db.Update<SYS_DICTIONARY_DETAIL>(new { ISDELETED = true, UPDATEON = DateTime.Now }, p => p.DICID == model.DICID && p.DETAILID == listdel[i].DETAILID);
                    }

                    // 编辑
                    for (int i = 0; i < listedit.Count; i++)
                    {
                        Db.Update<SYS_DICTIONARY_DETAIL>(listedit[i], p => p.DICID == model.DICID && p.DETAILID == listedit[i].DETAILID);
                    }

                    dbTrans.Commit();
                }

                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        // 新增字典 连带字典明细
        public object Any(AddDictionaryWithDetail request)
        {

            var dto = new MessageDto();
            var model = request.SysDictionaryModel;
            var listadd = request.ListAdd;

            if (string.IsNullOrEmpty(model.DICID))
            {
                dto.Flag = false;
                dto.Msg = "字典ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.DICNAME))
            {
                dto.Flag = false;
                dto.Msg = "字典名称不允许为空";
                return dto;
            }

            try
            {

                using (System.Data.IDbTransaction dbTrans = Db.OpenTransaction())
                {

                    // 新增进主表
                    Db.Insert<SYS_DICTIONARY>(model);

                    // 增加
                    for (int i = 0; i < listadd.Count; i++)
                    {
                        listadd[i].DICID = model.DICID;
                        Db.Insert<SYS_DICTIONARY_DETAIL>(listadd[i]);
                    }

                    dbTrans.Commit();
                }

                dto.Flag = true;
                dto.Msg = "新增成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }
        /////////////////////////// 以下是字典明细表接口实现 /////////////////////////////////


        public object Any(GetDictDetail request)
        {
            string DicId = request.DictId;
            var list = Db.Select<SYS_DICTIONARY_DETAIL>(c => c.DICID == DicId);
            if (list.Count > 0)
            {
                return list[0];
            }
            return new SYS_DICTIONARY_DETAIL();
        }

        public object Any(GetDictDetailList request)
        {
            var pagresult = new MiniUiGridModel<SYS_DICTIONARY_DETAIL>();

            var q = Db.From<SYS_DICTIONARY_DETAIL>()
            .Where(x => x.ISDELETED == false)
            .And(x => x.DICID == request.DicId)
            .OrderBy(x => x.ORDERNUM);
            var list = Db.Select<SYS_DICTIONARY_DETAIL>(q);

            pagresult.data = list;
            pagresult.total = list.Count;
            return pagresult;
        }

        public object Any(GetDictDetailList2 request)
        {
            var q = Db.From<SYS_DICTIONARY_DETAIL>()
            .Where(x => x.ISDELETED == false)
            .And(x => x.DICID == request.DicId)
            .OrderBy(x => x.ORDERNUM);
            var list = Db.Select<SYS_DICTIONARY_DETAIL>(q);
            return list;
        }


        public object Any(AddDictDetail request)
        {
            var dto = new MessageDto();
            var model = request.SysDictDetailModel;

            if (string.IsNullOrEmpty(model.DICID))
            {
                dto.Flag = false;
                dto.Msg = "字典ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.DETAILID))
            {
                dto.Flag = false;
                dto.Msg = "字典明细ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.DETAILNAME))
            {
                dto.Flag = false;
                dto.Msg = "字典名称不允许为空";
                return dto;
            }

            try
            {
                Db.Insert<SYS_DICTIONARY_DETAIL>(model);
                dto.Flag = true;
                dto.Msg = "新增成功";
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        public object Any(EditDictDetail request)
        {
            var dto = new MessageDto();
            var model = request.SysDictDetailModel;

            if (string.IsNullOrEmpty(model.DICID))
            {
                dto.Flag = false;
                dto.Msg = "字典ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.DETAILID))
            {
                dto.Flag = false;
                dto.Msg = "字典明细ID不允许为空";
                return dto;
            }
            if (string.Equals(model.DETAILNAME, ""))
            {
                dto.Flag = false;
                dto.Msg = "字典名称不允许为空";
                return dto;
            }


            try
            {
                var list = Db.Select<SYS_DICTIONARY_DETAIL>(c => c.DETAILID == model.DETAILID);
                if (list.Count == 0)
                {
                    dto.Flag = false;
                    dto.Msg = "您编辑的字典不存在，请刷新页面后重试";
                    return dto;
                }

                var editmodel = list[0];
                editmodel = AcexeService.ServiceInterface.Lib.AutoTransfer.GetModel<SYS_DICTIONARY_DETAIL>(editmodel, model);
                Db.Update<SYS_DICTIONARY_DETAIL>(editmodel);
                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }

        public object Any(DeleteDictDetail request)
        {
            var dto = new MessageDto();
            if (string.IsNullOrEmpty(request.DictDetailId))
            {
                dto.Flag = false;
                dto.Msg = "空记录";
                return dto;
            }
            var id = request.DictDetailId;
            try
            {
                Db.Update<SYS_DICTIONARY_DETAIL>();
                dto.Flag = true;
                dto.Msg = "删除成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
                throw;
            }
        }
    }
}
