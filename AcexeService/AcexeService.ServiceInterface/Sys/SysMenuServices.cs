﻿using AcexeService.Models;
using AcexeService.ServiceModel.Sys;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AcexeService.ServiceInterface
{
    [Authenticate]
    public class SysMenuServices : Service
    {
        public object Any(GetMenuList request)
        {
            var q = Db.From<SYS_MENU>()
            .Join<SYS_MENU, SYS_MODULE_MENU>((a, b) => (a.MENUID == b.MENUID))
            .Join<SYS_MODULE_MENU, SYS_MODULE>((a, b) => (a.MODULEID == b.MODULEID))
            .LeftJoin<SYS_MENU, SYS_FUNCTION>((a, b) => a.FUNCID == b.FUNCID)
            .Where(x => x.ISDELETED == false)
            .OrderBy(x => x.MENUORDERNUM);

            if (!string.IsNullOrEmpty(request.FuzzyMenuName))
            {
                q.And(x => x.MENUNAME.Contains(request.FuzzyMenuName));
            }
            if (!string.IsNullOrEmpty(request.Platform))
            {
                q.And<SYS_MODULE>(x => x.MODULEPLATFORM == request.Platform);
            }
            if (!string.IsNullOrEmpty(request.Module))
            {
                q.And<SYS_MODULE_MENU>(x => x.MODULEID == request.Module);
            }

            q.Select<SYS_MENU, SYS_FUNCTION, SYS_MODULE>((a, b, c) => new
            {
                a.MENUID,
                a.MENUNAME,
                a.MENUORDERNUM,
                a.MENUPARENTID,
                a.UPDATEBY,
                a.UPDATEON,
                a.CREATEON,
                a.CREATEBY,
                b.FUNCNAME,
                c.MODULENAME
            });

            var list = Db.Select<Dictionary<string, object>>(q);
            return list;
        }

        public object Any(GetMenu request)
        {
            string menuid = request.MenuId;

            var list = Db.Select<SYS_MENU>(c => c.MENUID == menuid);
            if (list.Count > 0)
            {
                return list[0];
            }
            return new SYS_MENU();
        }

        public object Any(GetMenu2 request)
        {
            string menuid = request.MenuId;

            var q = Db.From<SYS_MENU>()
            .LeftJoin<SYS_MENU, SYS_FUNCTION>((a, b) => (a.FUNCID == b.FUNCID && b.ISDELETED == false))
            .Where(a => a.ISDELETED == false)
            .And<SYS_MENU>(a => a.MENUID == menuid)
            .Select<SYS_MENU, SYS_FUNCTION>((a, b) => new
            {
                a,
                b.FUNCNAME
            });

            var list = Db.Select<Dictionary<string, object>>(q);
            if (list.Count > 0)
            {
                if (list[0]["FUNCNAME"] == null)
                {
                    list[0]["FUNCNAME"] = list[0]["FUNCID"];
                }
                return list[0];
            }
            return null;
        }

        public object Any(DeleteMenu request)
        {
            var dto = new MessageDto();
            try
            {
                //只作虚删除
                var menuid = request.MenuId;
                Db.ExecuteSql("UPDATE SYS_MENU SET ISDELETED = @isdeleted WHERE MENUID = @menuid", new { isdeleted = 1, menuid = menuid });
                dto.Flag = true;
                dto.Msg = "删除成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }

        public object Any(AddMenuDir request)
        {

            var dto = new MessageDto();

            if (string.IsNullOrEmpty(request.ModuleId))
            {
                dto.Flag = false;
                dto.Msg = "参数模块id为空";
                return dto;
            }

            try
            {
                string moduleid = request.ModuleId;
                string parentid = "M_" + moduleid;

                if (request.SysMenuModel.MENUORDERNUM == null)
                {
                    // 查找该级别下最大的排序号
                    int maxorder = Db.Scalar<SYS_MENU, int>(x => Sql.Max(x.MENUORDERNUM), x => x.MENUPARENTID == parentid && x.ISDELETED == false);
                    request.SysMenuModel.MENUORDERNUM = maxorder + 1;
                }

                using (System.Data.IDbTransaction dbTrans = Db.OpenTransaction())
                {
                    Db.Insert<SYS_MODULE_MENU>(new SYS_MODULE_MENU
                    {
                        MENUID = request.SysMenuModel.MENUID,
                        MODULEID = moduleid.ToString()
                    });

                    Db.Insert<SYS_MENU>(request.SysMenuModel);
                    dbTrans.Commit();
                }

                dto.Flag = true;
                dto.Msg = "新增成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
                return dto;
            }
        }

        public object Any(EditMenu request)
        {
            var dto = new MessageDto();
            var model = request.SysMenuModel;

            if (string.IsNullOrEmpty(model.MENUID))
            {
                dto.Flag = false;
                dto.Msg = "菜单ID不允许为空";
                return dto;
            }
            if (string.IsNullOrEmpty(model.MENUNAME))
            {
                dto.Flag = false;
                dto.Msg = "菜单名称不允许为空";
                return dto;
            }
            
            try
            {
                var list = Db.Select<SYS_MENU>(c => c.MENUID == model.MENUID);
                if (list.Count == 0)
                {
                    dto.Flag = false;
                    dto.Msg = "您编辑的菜单不存在，请刷新页面后重试";
                    return dto;
                }

                var editmodel = list[0];
                editmodel = AcexeService.ServiceInterface.Lib.AutoTransfer.GetModel<SYS_MENU>(editmodel, model);
                Db.Update<SYS_MENU>(editmodel);
                dto.Flag = true;
                dto.Msg = "修改成功";
                return dto;
            }
            catch (Exception ex)
            {
                dto.Flag = false;
                dto.Msg = ex.ToString();
            }
            return dto;
        }
    }
}