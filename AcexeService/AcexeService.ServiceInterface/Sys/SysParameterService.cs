﻿using AcexeService.Models;
using AcexeService.ServiceModel.Sys;
using ServiceStack;
using ServiceStack.OrmLite;

namespace AcexeService.ServiceInterface.Sys
{
    [Authenticate]
    public class SysParameterService : Service
    {
        /// <summary>
        /// 系统参数树型菜单
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetParameterCate request)
        {
            string parentid = request.ParentId;
            string id = request.Id;

            var q = Db.From<SYS_PARAMETER_CATE>().Where(x=>x.ISDELETED == false);

            if (!string.IsNullOrEmpty(id)) 
            {
              q.And(c => c.PARAMCATEID == id);
            }
            if (!string.IsNullOrEmpty(parentid))
            {
                q.And(c => c.PARAMCATEPARENTID == parentid);
            }

            q.OrderBy(x => x.PARAMCATEORDER);

            return Db.Select<SYS_PARAMETER_CATE>(q);
        }

        /// <summary>
        /// 系统参数树型菜单
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetParameter request)
        {
            string paramcateid = request.ParamCateId;
            string id = request.Id;
            string name = request.ParamName;

            var q = Db.From<SYS_PARAMETER>().Where(x => x.ISDELETED == false);

            if (!string.IsNullOrEmpty(id))
            {
                q.And(c => c.PARAMID == id);
            }
            if (!string.IsNullOrEmpty(paramcateid))
            {
                q.And(c => c.PARAMCATEID == paramcateid);
            }
            if (!string.IsNullOrEmpty(name))
            {
                q.And(c => c.PARAMNAME.Contains(name));
            }

            q.OrderBy(x => x.CREATEON);

            return Db.Select<SYS_PARAMETER>(q);
        }
        
    }
}
