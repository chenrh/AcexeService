﻿//
// 系统模块数据服务
//
// 陈日红@2017/2/9 13:36

using AcexeService.Models;
using AcexeService.ServiceModel.Sys;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;

namespace AcexeService.ServiceInterface
{
    [Authenticate]
    public class SysModuleService : Service
    {

        /// <summary>
        /// 系统模块数据服务
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        public object Any(GetModule request)
        {
            string platform = request.Platform;
            var q = Db.From<SYS_MODULE>()
            .Where(p => p.ISDELETED == false)
            .OrderBy(x => x.ORDERNUM)
            .Select<SYS_MODULE>(c => new { c.MODULEID, c.MODULENAME, c.MODULEPLATFORM });
            if (!string.IsNullOrEmpty(platform))
            {
                q.And(x => x.MODULEPLATFORM == platform);
            }
            return Db.Select<SYS_MODULE>(q);
        }
    }
}
