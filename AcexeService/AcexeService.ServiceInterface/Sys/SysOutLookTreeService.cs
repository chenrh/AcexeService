﻿//
// 陈日红@2017/02/07 12:21
//
// 主面板 左侧菜单界面 数据服务


using AcexeService.Models;
using AcexeService.ServiceModel.Sys;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace AcexeService.ServiceInterface.Sys
{
    // B/S 架构 左侧树型菜单 - 按权限范围展示


    [Api("根据用户ID得到他有访问权限的菜单")]
    [Route("/GetSysOutLookTreeData/{UserId}")]
    public class GetSysOutLookTreeData : IReturn<List<SYS_MENU>>
    {
        [ApiMember(Description = "用户ID", IsRequired = true)]
        public string UserId { get; set; }
    }

    [Authenticate]
    public class SysOutLookTreeService : Service
    {
        private void GetParents(List<SYS_MENU> source, List<SYS_MENU> result, string menuid)
        {
            var menumodel = source.FirstOrDefault(c => c.MENUID == menuid);

            if (menumodel == null || string.IsNullOrEmpty(menumodel.MENUID))
            {
                return;
            }

            if (result.Contains(menumodel))
            {
                return;
            }
            result.Add(menumodel);

            var parentId = menumodel.MENUPARENTID;
            GetParents(source, result, parentId);

        }


        private List<SYS_MENU> GetMenuSource()
        {
            List<SYS_MENU> rets = new List<SYS_MENU>();

            var modules = Db.Select<SYS_MODULE>(c => c.ISDELETED == false && c.MODULEPLATFORM == "BS").OrderBy(c => c.ORDERNUM).ToList();
            for (int i = 0; i < modules.Count; i++)
            {
                var item = modules[i];
                rets.Add(new SYS_MENU
                {
                    MENUNAME = item.MODULENAME,
                    MENUID = "M_" + item.MODULEID,
                    MENUORDERNUM = i
                });
            }

            var arrModules = modules.Select(c => c.MODULEID);
            var q = Db.From<SYS_MENU>()
                .Join<SYS_MODULE_MENU>((a, c) => a.MENUID == c.MENUID)
                .LeftJoin<SYS_MENU, SYS_FUNCTION>((a, b) => (a.FUNCID == b.FUNCID))
                .Where(x => x.ISDELETED == false)
                .And<SYS_MODULE_MENU>(c => Sql.In(c.MODULEID, arrModules))
                .OrderBy(x => x.MENUORDERNUM)
                .Select<SYS_MENU, SYS_FUNCTION>((a, b) => new
                {
                    a.MENUID,
                    a.MENUNAME,
                    a.MENUPARAM,
                    a.MENUPARENTID,
                    a.MENUSTYLE,
                    a.MENUORDERNUM,
                    UPDATEBY = b.FUNCURL
                });
            var list = Db.Select<SYS_MENU>(q);
            rets.AddRange(list);

            return rets;
        }




        public object Any(GetSysOutLookTreeData request)
        {
            if (string.IsNullOrEmpty(request.UserId)) { return new List<SYS_MENU>(); }

            List<SYS_MENU> list_all_menu = GetMenuSource();

            List<SYS_PERMISSIONS_FUNC> list_permis_menu = Lib.Permissions.Init(request.UserId).GetPermissionsMenu();
            List<SYS_MENU> list_menu = new List<SYS_MENU>();

            for (int i = 0; i < list_permis_menu.Count; i++)
            {
                var menu = list_permis_menu[i];

                GetParents(list_all_menu, list_menu, menu.FUNCID);
            }

            list_menu = list_menu.OrderBy(c => c.MENUORDERNUM).ToList();
            return list_menu;
        }
    }
}
