﻿using NUnit.Framework;
using AcexeService.ServiceModel;
using ServiceStack.Testing;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.Auth;
using ServiceStack.OrmLite;
using ServiceStack.Configuration;

namespace AcexeService.ServiceInterface.Tests
{
    [TestFixture]
    public class UnitTests
    {
        public readonly ServiceStackHost appHost;

        public UnitTests()
        {
            appHost = new BasicAppHost(typeof(MyServices).Assembly)
            {
                ConfigureContainer = container =>
                {
                    ServiceStack.Text.JsConfig.EmitLowercaseUnderscoreNames = false;
                    //string conn = "Password=bds260192108;User ID=bds260192108;Initial Catalog=bds260192108_db;Data Source=bds260192108.my3w.com;Pooling=true;";
                    string conn = "Password=abc123~;User ID=wms;Initial Catalog=WMS_DB2;Data Source=192.168.2.10;Pooling=true;";
                    
                    container.Register<IDbConnectionFactory>(c => new
                        OrmLiteConnectionFactory(conn, SqlServerDialect.Provider));

                    container.Register<IAuthRepository>(c =>
                        new OrmLiteAuthRepository(c.Resolve<IDbConnectionFactory>())
                        {
                            UseDistinctRoleTables = true//AppSettings.Get("UseDistinctRoleTables", true),
                        });

                    var authRepo = (OrmLiteAuthRepository)container.Resolve<IAuthRepository>();
                    MyServices.ResetUsers(authRepo);
                }
            }
            .Init();
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
            appHost.Dispose();
        }

        [Test]
        public void TestMethod1()
        {
            var service = appHost.Container.Resolve<MyServices>();

            var response = (HelloResponse)service.Any(new Hello { Name = "World" });

            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
        }
    }
}
