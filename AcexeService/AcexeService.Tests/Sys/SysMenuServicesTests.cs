﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcexeService.ServiceInterface;
using NUnit.Framework;
using AcexeService.Models;
namespace AcexeService.ServiceInterface.Tests
{
    [TestFixture()]
    public class SysMenuServicesTests
    {
        [Test()]
        public void GetMenuListTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysMenuServices>();
            var response = service.Any(new ServiceModel.Sys.GetMenuList()
            {
                Platform = "BS"
            });
            Assert.That(response, Is.EqualTo(null));
        }

        [Test()]
        public void AddMenuDirTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysMenuServices>();

            string funcid = "〖目录〗";
            string menuname = "测试目录";
            string moduleid = "10002";
            string menupid = "M_" + moduleid;

            var response = (MessageDto)service.Any(new ServiceModel.Sys.AddMenuDir()
            {
                ModuleId = moduleid,
                SysMenuModel = new SYS_MENU
                {
                    FUNCID = funcid,
                    MENUID = Guid.NewGuid().ToString(),
                    MENUNAME = menuname,
                    MENUPARENTID = menupid,
                    ISDELETED = false,
                    CREATEBY = "aa",
                    UPDATEBY = "aa",
                    CREATEON = DateTime.Now,
                    UPDATEON = DateTime.Now,
                    MENUORDERNUM = 100
                }
            });
            Assert.That(response.Flag, Is.EqualTo(true));
        }
    }
}
