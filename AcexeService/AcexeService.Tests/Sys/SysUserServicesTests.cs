﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcexeService.ServiceInterface;
using AcexeService.Models;
using NUnit.Framework;
using AcexeService.ServiceModel.Sys;

namespace AcexeService.ServiceInterface.Tests
{
    [TestFixture()]
    public class SysUserServicesTests
    {
        [Test()]
        public void GetUserTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysUserServices>();
            var response = (SYS_USER)service.Any(new ServiceModel.Sys.GetUser() { UserId = "CRH" });
            Assert.That(response.USERNAME, Is.EqualTo("张三"));
        }

         

        [Test()]
        public void AddUserTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysUserServices>();

            var model = new ServiceModel.Sys.AddUser();
            model.SysUserModel = new SYS_USER()
            {
                USERID = Guid.NewGuid().ToString(),
                USERNAME = "测试用户",
                USERGENDER = "女",
                CREATEBY = "陈日红",
                USERACCOUNT = "ACEXE3000",
                CREATEON = DateTime.Now
            };

            var dto = (MessageDto)service.Any(model);

            Assert.That(dto.Msg, Is.EqualTo("新增成功"));
        }

        [Test()]
        public void EditUserTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysUserServices>();

            var model = new ServiceModel.Sys.EditUser();
            model.SysUserModel = new SYS_USER()
            {
                USERID = "TEST_ID",
                USERNAME = "测试编辑"
            };

            var dto = (MessageDto)service.Any(model);

            Assert.That(dto.Msg, Is.EqualTo(null));
        }

        [Test()]
        public void DeleteUserTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysUserServices>();

            var model = new ServiceModel.Sys.DeleteUser();
            model.UserId = new List<string>(){
             "USER2002","USER2003","USER2004"
            };

            var dto = (MessageDto)service.Any(model);

            Assert.That(dto.Msg, Is.EqualTo(null));
        }
    }
}
