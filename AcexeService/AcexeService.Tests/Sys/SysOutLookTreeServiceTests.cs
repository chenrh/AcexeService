﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcexeService.ServiceInterface.Sys;
using NUnit.Framework;
using AcexeService.ServiceInterface.Tests;
using AcexeService.Models;
using ServiceStack.OrmLite;
namespace AcexeService.ServiceInterface.Sys.Tests
{
    [TestFixture()]
    public class SysOutLookTreeServiceTests
    {
        [Test()]
        public void GetSysParameterCateTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<SysParameterService>();

            var response = (List<SYS_PARAMETER_CATE>)service.Any(new ServiceModel.Sys.GetParameterCate());

            Assert.That(response.Count, Is.GreaterThanOrEqualTo(1));
        }
    }
}
