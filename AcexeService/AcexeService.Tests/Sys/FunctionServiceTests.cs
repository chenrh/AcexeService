﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcexeService.ServiceInterface;
using NUnit.Framework;
using AcexeService.Models;
namespace AcexeService.ServiceInterface.Tests
{
    [TestFixture()]
    public class FunctionServiceTests
    {
        [Test()]
        public void AddFunctionTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<FunctionService>();

            var response = (MessageDto)service.Any(new ServiceModel.Sys.AddFunction()
            {
                SysFuncModel = new Models.SYS_FUNCTION
                {
                    FUNCID = Guid.NewGuid().ToString(),
                    FUNCNAME = "测试功能",
                    FUNCURL = "../../home/index",
                    ISDELETED = false,
                    CREATEBY = "陈日红",
                    UPDATEBY = "陈日红",
                    CREATEON = DateTime.Now,
                    UPDATEON = DateTime.Now
                }
            });



            Assert.That(response.Msg, Is.EqualTo("新增成功"));
        }

        [Test()]
        public void EditFunctionTest()
        {
            var appHost = new UnitTests().appHost;
            var service = appHost.Container.Resolve<FunctionService>();

            var response = (MessageDto)service.Any(new ServiceModel.Sys.EditFunction()
            {
                SysFuncModel = new Models.SYS_FUNCTION
                {
                    FUNCID = "ae852ca1-e549-4112-893d-dea9dc13492e",
                    FUNCNAME = "测试功能-编辑222",
                    FUNCURL = "../asdasdasd",
                    ISDELETED = false,
                    UPDATEBY = "陈日红2",
                    UPDATEON = DateTime.Now
                }
            });

            Assert.That(response.Msg, Is.EqualTo("修改成功"));
        }
    }
}
