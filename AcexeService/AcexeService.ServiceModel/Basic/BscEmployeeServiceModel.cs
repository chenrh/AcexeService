﻿using ServiceStack;
using System.Collections.Generic;
using AcexeService.Models;

namespace AcexeService.ServiceModel.Basic
{

    [Api("根据员工ID获取员工实体")]
    [Route("/GetEmployee/{EmployeeId}")]
    public class GetEmployee : IReturn<BSC_EMPLOYEE>
    {
        [ApiMember(Description = "员工的ID", IsRequired = true)]
        public string EmployeeId { get; set; }
    }

    [Api("根据分页信息返回分好页的员工数据")]
    public class GetEmployeeList : IReturn<MiniUiGridModel<BSC_EMPLOYEE>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public BSC_EMPLOYEE BscEmpModel { get; set; }

        /// <summary>
        /// 模糊查询员工帐号和员工姓名
        /// </summary>
        [ApiMember(Description = "过滤条件：模糊查询员工工号和员工姓名", IsRequired = true)]
        public string FuzzyEmployeeName { get; set; }
    }

    [Api("新增员工")]
    public class AddEmployee : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public BSC_EMPLOYEE BscEmpModel { get; set; }
    }

    [Api("编辑员工")]
    public class EditEmployee : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public BSC_EMPLOYEE BscEmpModel { get; set; }
    }

    [Api("删除员工")]
    public class DeleteEmployee : IReturn<MessageDto>
    {
        [ApiMember(Description = "员工ID", IsRequired = true)]
        public string EmployeeId { get; set; }

        [ApiMember(Description = "由谁删除", IsRequired = true)]
        public string DeleteBy { get; set; }
    }
}
