﻿using ServiceStack;
using System.Collections.Generic;
using AcexeService.Models;

namespace AcexeService.ServiceModel.Basic
{

    [Api("根据岗位ID获取岗位实体")]
    [Route("/GetJobName/{JobId}")]
    public class GetJobName : IReturn<BSC_JOBNAME>
    {
        [ApiMember(Description = "岗位的ID", IsRequired = true)]
        public string JobId { get; set; }
    }

    [Api("根据岗位ID获取岗位实体")]
    [Route("/GetJobNameWithOrg/{JobId}")]
    public class GetJobNameWithOrg : IReturn<StrObjDict>
    {
        [ApiMember(Description = "岗位的ID", IsRequired = true)]
        public string JobId { get; set; }
    }

    [Api("根据分页信息返回分好页的岗位数据")]
    public class GetJobNameList : IReturn<MiniUiGridModel<BSC_JOBNAME>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public BSC_JOBNAME BscEmpModel { get; set; }

        [ApiMember(Description = "过滤条件：模糊查询岗位名称", IsRequired = true)]
        public string FuzzyJobNameName { get; set; }
    }

    [Api("新增岗位")]
    public class AddJobName : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public BSC_JOBNAME BscEmpModel { get; set; }
    }

    [Api("编辑岗位")]
    public class EditJobName : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public BSC_JOBNAME BscEmpModel { get; set; }
    }

    [Api("删除岗位")]
    public class DeleteJobName : IReturn<MessageDto>
    {
        [ApiMember(Description = "岗位ID", IsRequired = true)]
        public string JobId { get; set; }

        [ApiMember(Description = "由谁删除", IsRequired = true)]
        public string DeleteBy { get; set; }
    }

    [Api("部门对应岗位")]
    [Route("/GetJobByOrgId/{OrgId}")]
    public class GetJobByOrgId : IReturn<List<StrObjDict>>
    {
        [ApiMember(Description = "组织部门ID", IsRequired = true)]
        public string OrgId { get; set; }
    }

    [Api("岗位对应部门")]
    [Route("/GetOrganizationByJobId/{JobId}")]
    public class GetOrganizationByJobId : IReturn<List<StrObjDict>>
    {
        [ApiMember(Description = "岗位ID", IsRequired = true)]
        public string JobId { get; set; }
    }

    [Api("增加岗位对应部门")]
    public class AddPairOrgJob : IReturn<MessageDto>
    {
        [ApiMember(Description = "部门ID-岗位ID实体", IsRequired = true)]
        public BSC_ORG_JOB BscOrgJobModel { get; set; }
    }

    [Api("增加岗位对应部门(batch)")]
    public class AddListPairOrgJob : IReturn<MessageDto>
    {
        [ApiMember(Description = "部门ID-岗位ID实体", IsRequired = true)]
        public List<BSC_ORG_JOB> ListBscOrgJob { get; set; }
    }

    [Api("删除岗位对应部门")]
    public class DeletePairOrgJob : IReturn<MessageDto>
    {
        [ApiMember(Description = "部门ID-岗位ID实体", IsRequired = true)]
        public BSC_ORG_JOB BscOrgJobModel { get; set; }
    }
}
