﻿//
// 陈日红@2017/02/17 15:58
//
// 仓库库区 服务接口

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.WareHouse
{

    [Api("根据仓库库区ID获取仓库库区实体")]
    [Route("/GetWareHouseAreaArea/{AreaId}")]
    public class GetWareHouseArea : IReturn<WARE_WAREHOUSE_AREA>
    {
        [ApiMember(Description = "仓库库区ID", IsRequired = true)]
        public string AreaId { get; set; }
    }

    [Api("根据分页信息返回分好页的仓库库区数据")]
    public class GetWareHouseAreaList : IReturn<MiniUiGridModel<WARE_WAREHOUSE_AREA>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public WARE_WAREHOUSE_AREA WareHouseAreaModel { get; set; }
    }

    [Api("新增仓库库区")]
    public class AddWareHouseArea : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public WARE_WAREHOUSE_AREA WareHouseAreaModel { get; set; }
    }

    [Api("编辑仓库库区")]
    public class EditWareHouseArea : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public WARE_WAREHOUSE_AREA WareHouseAreaModel { get; set; }
    }

    [Api("删除仓库库区(单个)")]
    public class DeleteWareHouseArea : IReturn<MessageDto>
    {
        [ApiMember(Description = "仓库库区ID", IsRequired = true)]
        public string WareAreaId { get; set; }

        [ApiMember(Description = "被谁删除", IsRequired = true)]
        public string UpdateBy { get; set; }
    }

    [Api("删除仓库库区(批量)")]
    public class DeleteWareHouseAreas : IReturn<MessageDto>
    {
        [ApiMember(Description = "仓库库区ID", IsRequired = true)]
        public List<string> ListWareAreaId { get; set; }

        [ApiMember(Description = "被谁删除", IsRequired = true)]
        public string UpdateBy { get; set; }
    }
}
