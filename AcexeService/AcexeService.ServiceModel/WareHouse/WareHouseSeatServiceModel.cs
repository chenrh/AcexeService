﻿//
// 陈日红@2017/02/17 16:10
//
// 仓库库位 服务接口

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.WareHouse
{

    [Api("根据仓库库位ID获取仓库库位实体")]
    [Route("/GetWareHouseSeat/{SeatId}")]
    public class GetWareHouseSeat : IReturn<WARE_WAREHOUSE_SEAT>
    {
        [ApiMember(Description = "仓库库位ID", IsRequired = true)]
        public string SeatId { get; set; }
    }

    [Api("根据分页信息返回分好页的仓库库位数据")]
    public class GetWareHouseSeatList : IReturn<MiniUiGridModel<WARE_WAREHOUSE_SEAT>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public WARE_WAREHOUSE_SEAT WareHouseSeatModel { get; set; }
    }

    [Api("新增仓库库位")]
    public class AddWareHouseSeat : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public WARE_WAREHOUSE_SEAT WareHouseSeatModel { get; set; }
    }

    [Api("编辑仓库库位")]
    public class EditWareHouseSeat : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public WARE_WAREHOUSE_SEAT WareHouseSeatModel { get; set; }
    }

    [Api("删除仓库库位(单个)")]
    public class DeleteWareHouseSeat : IReturn<MessageDto>
    {
        [ApiMember(Description = "仓库库位ID", IsRequired = true)]
        public string WareSeatId { get; set; }

        [ApiMember(Description = "被谁删除", IsRequired = true)]
        public string UpdateBy { get; set; }
    }

    [Api("删除仓库库位(批量)")]
    public class DeleteWareHouseSeats : IReturn<MessageDto>
    {
        [ApiMember(Description = "仓库库位ID", IsRequired = true)]
        public List<string> ListWareSeatId { get; set; }

        [ApiMember(Description = "被谁删除", IsRequired = true)]
        public string UpdateBy { get; set; }
    }
}
