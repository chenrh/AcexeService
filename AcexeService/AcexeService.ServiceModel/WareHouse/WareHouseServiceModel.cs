﻿//
// 陈日红@2017/02/17 15:58
//
// 仓库 服务接口

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.WareHouse
{

    [Api("根据仓库ID获取仓库实体")]
    [Route("/GetWareHouse/{WareId}")]
    public class GetWareHouse : IReturn<WARE_WAREHOUSE>
    {
        [ApiMember(Description = "仓库ID", IsRequired = true)]
        public string WareId { get; set; }
    }

    [Api("直接返回仓库数据")]
    [Route("/GetTreeGridWareHouse")]
    public class GetTreeGridWareHouse : IReturn<List<WARE_WAREHOUSE>>
    {

    }

    [Api("根据分页信息返回分好页的仓库数据")]
    public class GetWareHouseList : IReturn<MiniUiGridModel<WARE_WAREHOUSE>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public WARE_WAREHOUSE WareHouseModel { get; set; }

        [ApiMember(Description = "过滤条件：模糊查询仓库名称", IsRequired = true)]
        public string FuzzyWareName { get; set; }
    }

    [Api("新增仓库")]
    public class AddWareHouse : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public WARE_WAREHOUSE WareHouseModel { get; set; }
    }

    [Api("编辑仓库")]
    public class EditWareHouse : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public WARE_WAREHOUSE WareHouseModel { get; set; }
    }

    [Api("删除仓库(单个)")]
    public class DeleteWareHouse : IReturn<MessageDto>
    {
        [ApiMember(Description = "仓库ID", IsRequired = true)]
        public string WareId { get; set; }

        [ApiMember(Description = "被谁删除", IsRequired = true)]
        public string UpdateBy { get; set; }
    }

    [Api("删除仓库(批量)")]
    public class DeleteWareHouses : IReturn<MessageDto>
    {
        [ApiMember(Description = "仓库ID", IsRequired = true)]
        public List<string> ListWareId { get; set; }

        [ApiMember(Description = "被谁删除", IsRequired = true)]
        public string UpdateBy { get; set; }
    }
}
