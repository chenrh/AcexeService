﻿using AcexeService.Models;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.ServiceModel.Organization
{
    [Api("根据组织类别ID获得组织数据")]
    [Route("/GetOrganizationList")]
    [Route("/GetOrganizationList/{CateId}")]
    public class GetOrganizationList : IReturn<List<StrObjDict>>
    {
        [ApiMember(Description = "组织类别ID", IsRequired = true)]
        public string CateId { get; set; }
    }

    [Api("根据组织ID获取组织实体")]
    [Route("/GetOrganization/{OrgId}")]
    public class GetOrganization : IReturn<ORG_ORGANIZATION>
    {
        [ApiMember(Description = "组织Id", IsRequired = true)]
        public string OrgId { get; set; }
    }

    [Api("根据分页信息返回分好页的组织数据")]
    public class GetOrganizationPageList : IReturn<MiniUiGridModel<StrObjDict>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public ORG_ORGANIZATION ORGANIZATIONModel { get; set; }
    }

    [Api("新增组织")]
    public class AddOrganization : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public ORG_ORGANIZATION SysOrganizationModel { get; set; }
    }

    [Api("编辑组织")]
    public class EditOrganization : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public ORG_ORGANIZATION SysOrganizationModel { get; set; }
    }

    [Api("删除组织")]
    public class DeleteOrganization : IReturn<MessageDto>
    {
        [ApiMember(Description = "组织ID", IsRequired = true)]
        public List<string> OrganizationId { get; set; }
    }
}
