﻿using AcexeService.Models;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.ServiceModel.Sys
{
    [Api("根据分页信息返回分好页的用户数据")]
    public class GetUserListByGroupId : IReturn<MiniUiGridModel<SYS_USER>>
    {
        [ApiMember(Description = "用户组ID", IsRequired = true)]
        public string GroupId { get; set; }


        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public SYS_USER SysUserModel { get; set; }

        /// <summary>
        /// 模糊查询用户帐号和用户姓名
        /// </summary>
        [ApiMember(Description = "过滤条件：模糊查询用户帐号和用户姓名", IsRequired = false)]
        public string FuzzyUserNameUserAccount { get; set; }
    }

}
