﻿//
// 陈日红@2017-02-08 14:12
//
// 系统参数服务

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.Sys
{
    [Api("根据查询参数获取系统参数类别")]
    [Route("/GetParameterCate/ParentId/{ParentId}")]
    [Route("/GetParameterCate/{Id}")]
    [Route("/GetParameterCate")]
    public class GetParameterCate : IReturn<List<SYS_PARAMETER_CATE>>
    {
        public string ParentId { get; set; }
        public string Id { get; set; }
    }

    [Api("根据系统参数类别/系统参数名称获取系统参数")]
    [Route("/GetParameter/Cate/{ParamCateId}")]
    [Route("/GetParameter/Name/{ParamName}")]
    [Route("/GetParameter/{Id}")]
    [Route("/GetParameter")]
    public class GetParameter : IReturn<List<SYS_PARAMETER>>
    {
        public string ParamCateId { get; set; }
        public string ParamName { get; set; }
        public string Id { get; set; }
    }
}
