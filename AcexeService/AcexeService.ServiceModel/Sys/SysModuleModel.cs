﻿//
// 陈日红@2017-02-09 13:12
//
// 根据查询参数获取系统模块数据

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.Sys
{
    [Api("根据查询参数获取系统模块数据")]
    [Route("/GetModule/{Platform}")]
    [Route("/GetModule")]
    public class GetModule : IReturn<List<SYS_MODULE>>
    {
        public string Platform { get; set; }

    }

}
