﻿//
// 陈日红@2017/02/07 09:02
//
// 菜单管理 服务接口

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.Sys
{

    [Api("根据分页信息返回分好页的菜单数据")]
    [Route("/GetMenuList")]
    [Route("/GetMenuList/{Platform}")]
    [Route("/GetMenuList/{Platform}/{Module}")]
    [Route("/GetMenuList/Name/{FuzzyMenuName}")]
    public class GetMenuList : IReturn<MiniUiGridModel<StrObjDict>>
    {
        [ApiMember(Description = "过滤条件：模糊查询菜单名称", IsRequired = false)]
        public string FuzzyMenuName { get; set; }

        [ApiMember(Description = "过滤条件：平台", IsRequired = true)]
        public string Platform { get; set; }

        [ApiMember(Description = "过滤条件：模块", IsRequired = false)]
        public string Module { get; set; }
    }

    [Api("编辑菜单")]
    public class EditMenu : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_MENU SysMenuModel { get; set; }
    }

    [Api("删除菜单(单个)")]
    public class DeleteMenu : IReturn<MessageDto>
    {
        [ApiMember(Description = "菜单ID", IsRequired = true)]
        public string MenuId { get; set; }
    }

    [Api("根据菜单ID获取菜单实体")]
    [Route("/GetMenu/{MenuId}")]
    public class GetMenu : IReturn<SYS_MENU>
    {
        [ApiMember(Description = "菜单的ID", IsRequired = true)]
        public string MenuId { get; set; }
    }

    [Api("根据菜单ID获取菜单实体，功能名称，模块名称等")]
    [Route("/GetMenu2/{MenuId}")]
    public class GetMenu2 : IReturn<StrObjDict>
    {
        [ApiMember(Description = "菜单的ID", IsRequired = true)]
        public string MenuId { get; set; }
    }

    [Api("新增菜单的目录（不对应功能）")]
    public class AddMenuDir : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_MENU SysMenuModel { get; set; }

        public string ModuleId { get; set; }
    }

}
