﻿using AcexeService.Models;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.ServiceModel.Sys
{
    #region 权限表记录CRUD

    [Api("根据权限ID获取权限实体")]
    [Route("/GetSysPermission/{PermissionId}")]
    public class GetSysPermission : IReturn<SYS_PERMISSIONS>
    {
        [ApiMember(Description = "用户权限的ID", IsRequired = true)]
        public string PermissionId { get; set; }
    }
    [Api("查询权限数据")]

    [Route("/GetSysPermissionList")]
    public class GetSysPermissionList : IReturn<List<SYS_PERMISSIONS>>
    {

    }
    [Api("新增权限")]
    public class AddSysPermission : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_PERMISSIONS SysPermissionModel { get; set; }
    }

    [Api("编辑权限")]
    public class EditSysPermission : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_PERMISSIONS SysPermissionModel { get; set; }
    }

    [Api("删除权限")]
    public class DeleteSysPermission : IReturn<MessageDto>
    {
        [ApiMember(Description = "权限ID", IsRequired = true)]
        public string SysPermissionId { get; set; }
    }

    #endregion

    [Api("根据权限ID返回分好页的功能数据")]
    public class GetFunctionListByPermisId : IReturn<MiniUiGridModel<SYS_FUNCTION>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public SYS_FUNCTION SysFuncModel { get; set; }

        [ApiMember(Description = "过滤条件：模糊查询功能名称", IsRequired = true)]
        public string FuzzyFuncName { get; set; }

        [ApiMember(Description = "权限ID", IsRequired = true)]
        public string PermissionId { get; set; }
    }

    [Api("根据权限ID返回分用户组数据")]
    [Route("/GetUserGroupListByPermisId/{PermissionId}")]
    public class GetUserGroupListByPermisId : IReturn<List<SYS_USERGROUP>>
    {
        [ApiMember(Description = "权限ID", IsRequired = true)]
        public string PermissionId { get; set; }
    }

    [Api("根据权限ID获得组织数据")]
    [Route("/GetOrganizationListByPermisId/{PermisId}")]
    [Route("/GetOrganizationListByPermisId/{PermisId}/{CateId}")]
    public class GetOrganizationListByPermisId : IReturn<List<StrObjDict>>
    {
        [ApiMember(Description = "权限ID", IsRequired = true)]
        public string PermisId { get; set; }

        [ApiMember(Description = "组织类别ID", IsRequired = true)]
        public string CateId { get; set; }
    }

    [Api("更新功能权限到权限表")]
    public class UpdatePermissionsFunction : IReturn<MessageDto>
    {
        [ApiMember(Description = "菜单id", IsRequired = true)]
        public string MenuId { get; set; }

        [ApiMember(Description = "权限id", IsRequired = true)]
        public string PermisId { get; set; }

        [ApiMember(Description = "是否叶子菜单", IsRequired = true)]
        public bool IsLeaf { get; set; }

        [ApiMember(Description = "是否选中了", IsRequired = true)]
        public bool IsChecked { get; set; }
    }

    [Api("更新组织架构权限到权限表")]
    public class UpdatePermissionsOrganization : IReturn<MessageDto>
    {
        [ApiMember(Description = "组织id", IsRequired = true)]
        public string OrgId { get; set; }

        [ApiMember(Description = "权限id", IsRequired = true)]
        public string PermisId { get; set; }

        [ApiMember(Description = "是否删除", IsRequired = true)]
        public bool IsDeleted { get; set; }

    }
}
