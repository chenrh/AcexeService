﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AcexeService.Models;

namespace AcexeService.ServiceModel.Sys
{
     
    [Api("根据用户ID获取用户实体")]
    [Route("/GetUserSession/{UserAccount}")]
    public class GetUserSession : IReturn<SYS_USER>
    {
        [ApiMember(Description = "用户帐号", IsRequired = true)]
        public string UserAccount { get; set; }
    }

    [Api("根据用户ID获取用户实体")]
    [Route("/GetUser/{UserId}")]
    public class GetUser : IReturn<SYS_USER>
    {
        [ApiMember(Description = "用户的ID", IsRequired = true)]
        public string UserId { get; set; }
    }

    [Api("根据分页信息返回分好页的用户数据")]
    public class GetUserList : IReturn<MiniUiGridModel<SYS_USER>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public SYS_USER SysUserModel { get; set; }

        /// <summary>
        /// 模糊查询用户帐号和用户姓名
        /// </summary>
        [ApiMember(Description = "过滤条件：模糊查询用户帐号和用户姓名", IsRequired = true)]
        public string FuzzyUserNameUserAccount { get; set; }
    }

    [Api("新增用户")]
    public class AddUser : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_USER SysUserModel { get; set; }
    }

    [Api("编辑用户")]
    public class EditUser : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_USER SysUserModel { get; set; }
    }

    [Api("删除用户")]
    public class DeleteUser : IReturn<MessageDto>
    {
        [ApiMember(Description = "用户ID", IsRequired = true)]
        public List<string> UserId { get; set; }
    }
}
