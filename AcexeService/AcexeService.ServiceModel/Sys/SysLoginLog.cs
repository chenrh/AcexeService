﻿
using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.Sys
{

    [Api("根据查询参数获取登录日志")]
    [Route("/GetLoginLog/{UserId}/{Size}")]
    [Route("/GetLoginLog/{UserId}")]
    [Route("/GetLoginLog")]
    [Authenticate]
    public class GetLoginLog : IReturn<List<StrObjDict>>
    {
        public int? Size { get; set; }
        public string UserId { get; set; }
    }
}
