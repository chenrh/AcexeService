﻿using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.Sys
{
    [Api("根据字典ID获取字典实体")]
    [Route("/GetDictionary/{DicId}")]
    public class GetDictionary : IReturn<SYS_DICTIONARY>
    {
        [ApiMember(Description = "字典的ID", IsRequired = true)]
        public string DicId { get; set; }
    }

    [Api("根据分页信息返回分好页的字典数据")]
    public class GetDictionaryList : IReturn<MiniUiGridModel<SYS_DICTIONARY>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = false)]
        public SYS_DICTIONARY SysDictionaryModel { get; set; }

        /// <summary>
        /// 模糊查询字典帐号和字典姓名
        /// </summary>
        [ApiMember(Description = "过滤条件：模糊查询字典ID和字典名称", IsRequired = false)]
        public string FuzzyDictionary { get; set; }
    }

    [Api("新增字典")]
    public class AddDictionary : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_DICTIONARY SysDictionaryModel { get; set; }
    }

    [Api("编辑字典")]
    public class EditDictionary : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_DICTIONARY SysDictionaryModel { get; set; }
    }

    [Api("删除字典")]
    public class DeleteDictionary : IReturn<MessageDto>
    {
        [ApiMember(Description = "字典ID", IsRequired = true)]
        public List<string> SysDictionaryId { get; set; }
    }

    [Api("编辑字典(连带字典明细)")]
    public class EditDictionaryWithDetail : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_DICTIONARY SysDictionaryModel { get; set; }

        [ApiMember(Description = "要增加的明细", IsRequired = false)]
        public List<SYS_DICTIONARY_DETAIL> ListAdd { get; set; }
        [ApiMember(Description = "要删除的明细", IsRequired = false)]
        public List<SYS_DICTIONARY_DETAIL> ListDelete { get; set; }
        [ApiMember(Description = "要修改的明细", IsRequired = false)]
        public List<SYS_DICTIONARY_DETAIL> ListModify { get; set; }
    }

    [Api("新增字典(连带字典明细)")]
    public class AddDictionaryWithDetail : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_DICTIONARY SysDictionaryModel { get; set; }

        [ApiMember(Description = "要增加的明细", IsRequired = false)]
        public List<SYS_DICTIONARY_DETAIL> ListAdd { get; set; }
    }

    /////////////////////////// 以下是字典明细表接口定义 /////////////////////////////////

    [Api("根据字典ID,取明细列表")]
    [Route("/GetDictDetailList/{DicId}")]
    public class GetDictDetailList : IReturn<MiniUiGridModel<SYS_DICTIONARY_DETAIL>>
    {
        [ApiMember(Description = "字典的ID", IsRequired = true)]
        public string DicId { get; set; }
    }

    [Api("根据字典ID,取明细列表")]
    [Route("/GetDictDetailList2/{DicId}")]
    public class GetDictDetailList2 : IReturn<List<SYS_DICTIONARY_DETAIL>>
    {
        [ApiMember(Description = "字典的ID", IsRequired = true)]
        public string DicId { get; set; }
    }

    [Api("根据字典ID,明细ID获取字典实体")]
    [Route("/GetDictDetail/{DictId}/{DictDetailId}")]
    public class GetDictDetail : IReturn<MessageDto>
    {
        [ApiMember(Description = "字典ID", IsRequired = true)]
        public string DictId { get; set; }

        [ApiMember(Description = "字典明细ID", IsRequired = true)]
        public string DictDetailId { get; set; }
    }

    [Api("新增字典明细")]
    public class AddDictDetail : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_DICTIONARY_DETAIL SysDictDetailModel { get; set; }
    }

    [Api("编辑字典明细")]
    public class EditDictDetail : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_DICTIONARY_DETAIL SysDictDetailModel { get; set; }
    }

    [Api("删除字典明细")]
    public class DeleteDictDetail : IReturn<MessageDto>
    {
        [ApiMember(Description = "字典ID", IsRequired = true)]
        public string DictDetailId { get; set; }
    }
}
