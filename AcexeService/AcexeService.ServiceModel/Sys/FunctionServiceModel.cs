﻿//
// 陈日红@2017/02/07 09:02
//
// 功能模块 服务接口

using ServiceStack;
using AcexeService.Models;
using System.Collections.Generic;

namespace AcexeService.ServiceModel.Sys
{

    [Api("根据功能ID获取功能实体")]
    [Route("/GetFunction/{FuncId}")]
    public class GetFunction : IReturn<SYS_FUNCTION>
    {
        [ApiMember(Description = "功能ID", IsRequired = true)]
        public string FuncId { get; set; }
    }

    [Api("根据分页信息返回分好页的功能数据")]
    public class GetFunctionList : IReturn<MiniUiGridModel<SYS_FUNCTION>>
    {
        [ApiMember(Description = "分页参数", IsRequired = true)]
        public PageParam Page { get; set; }

        [ApiMember(Description = "过滤条件：等于", IsRequired = true)]
        public SYS_FUNCTION SysFuncModel { get; set; }

        [ApiMember(Description = "过滤条件：模糊查询功能名称", IsRequired = true)]
        public string FuzzyFuncName { get; set; }
    }

    [Api("新增功能")]
    public class AddFunction : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_FUNCTION SysFuncModel { get; set; }
    }

    [Api("编辑功能")]
    public class EditFunction : IReturn<MessageDto>
    {
        [ApiMember(Description = "实体数据", IsRequired = true)]
        public SYS_FUNCTION SysFuncModel { get; set; }
    }

    [Api("删除功能(单个)")]
    public class DeleteFunction : IReturn<MessageDto>
    {
        [ApiMember(Description = "功能ID", IsRequired = true)]
        public string FuncId { get; set; }
    }

    [Api("删除功能(批量)")]
    public class DeleteFunctions : IReturn<MessageDto>
    {
        [ApiMember(Description = "功能ID", IsRequired = true)]
        public List<string> ListFuncId { get; set; }
    }

    [Api("发布功能到菜单")]
    public class DeployFunction : IReturn<MessageDto>
    {
        [ApiMember(Description = "功能ID", IsRequired = true)]
        public string FuncId { get; set; }

        [ApiMember(Description = "菜单", IsRequired = true)]
        public SYS_MENU SysMenuModel { get; set; }
    }
}
