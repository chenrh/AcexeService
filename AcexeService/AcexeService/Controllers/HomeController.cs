﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using AcexeService.Models;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Mvc;
using ServiceStack.OrmLite;
using AcexeService.ServiceInterface;
using System.Text;
using System.Security.Cryptography;

namespace AcexeService.Controllers
{
    public class HomeController : ServiceStackController
    {
        public HomeViewModel GetViewModel()
        {

            var response = new HomeViewModel { Session = SessionAs<CustomUserSession>() };

            if (response.Session.UserAuthId != null)
            {
                //var userAuthId = int.Parse(response.Session.UserAuthId);
                //response.UserAuths = Db.Select<UserAuth>(x => x.Id == userAuthId);
                //response.UserAuthDetails = Db.Select<UserAuthDetails>(x => x.UserAuthId == userAuthId);
            }
            return response;
        }

        public ActionResult Index()
        {
            return View(GetViewModel());
        }

        public ActionResult Login(string userName, string password, string redirect = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var authService = ResolveService<AuthenticateService>())
                    {
                        /*
                         The ss-opt cookie stores the users preference on 
                         whether they want their current session 
                         to be temporary (ss-opt=temp 临时的) or permanent (ss-opt=perm 永久的) 
                         - i.e. to RememberMe or not 
                         - The Default is Temporary
                         */

                        byte[] result = Encoding.Default.GetBytes(password.Trim());
                        MD5 md5 = new MD5CryptoServiceProvider();
                        byte[] output = md5.ComputeHash(result);
                        string md5password = BitConverter.ToString(output).Replace("-", "");

                        var response = authService.Authenticate(new Authenticate
                        {
                            provider = CredentialsAuthProvider.Name,
                            UserName = userName,
                            Password = md5password
                        });

                        // add ASP.NET auth cookie
                        FormsAuthentication.SetAuthCookie(userName, true);

                        return Redirect(string.IsNullOrEmpty(redirect) ? "/" : redirect);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return View("Index", GetViewModel());
        }

        public ActionResult Logout()
        {
            using (var authService = ResolveService<AuthenticateService>())
            {
                authService.Authenticate(new Authenticate { provider = "logout" });
            }

            FormsAuthentication.SignOut();

            return Redirect("/");
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}