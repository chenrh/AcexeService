﻿using System.Web.Mvc;
using AcexeService.ServiceInterface;
using ServiceStack;
using ServiceStack.Mvc;

namespace AcexeService.Controllers
{
    [Authenticate]
    public class AuthOnlyController : ServiceStackController 
    {
        public ActionResult Index()
        {
            var session = SessionAs<CustomUserSession>();
            return View(session);
        }         
    }
}