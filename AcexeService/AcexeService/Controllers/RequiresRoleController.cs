﻿using System.Web.Mvc;
using AcexeService.ServiceInterface;
using ServiceStack;
using ServiceStack.Mvc;

namespace AcexeService.Controllers
{
    [RequiredRole("TheRole")]
    public class RequiresRoleController : ServiceStackController 
    {
        public ActionResult Index()
        {
            var session = SessionAs<CustomUserSession>();
            return View(session);
        }
    }
}