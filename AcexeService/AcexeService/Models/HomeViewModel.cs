﻿using System.Collections.Generic;
using AcexeService.ServiceInterface;
using ServiceStack.Auth;

namespace AcexeService.Models
{
    public class HomeViewModel
    {
        public CustomUserSession Session { get; set; }
        public List<UserAuth> UserAuths { get; set; }
        public List<UserAuthDetails> UserAuthDetails { get; set; }
    }
}