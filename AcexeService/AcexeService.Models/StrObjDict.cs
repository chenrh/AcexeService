﻿using System;
using System.Collections.Generic;

namespace AcexeService.Models
{
    /*
     *   在 使用字典 Dictionary<string, object> 时
     *   客户端更新引用时  
     *   总是更新为 Dictionary
     *   导致生成报错
     *   所以使用本类代替 Dictionary<string, object> 写法
     *   
     *   陈日红@2017-02-09 11:30
     */

    /*
     *   一般在哪里会用到:
     *       比如 用到联合查询时，返回结果为字典数据时
     *       客户端能直接用
     *       示例： GetLogLoginService.cs
     */
    [Serializable]
    public class StrObjDict : Dictionary<string, object>
    {

    }
}
