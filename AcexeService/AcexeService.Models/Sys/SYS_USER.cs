﻿using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 类SYS_USER。
    /// </summary>
    [Serializable]
    public partial class SYS_USER
    {
        private string _userid;
        private string _useraccount;
        private string _userpwd;
        private string _username;
        private string _usergender;
        private string _useremail;
        private string _userphonenumber;
        private string _useridnumber;
        private DateTime? _userbirthday;
        private string _userphoto;
        private bool _isdeleted = false;
        private bool _isvalid = true;
        private DateTime? _createon;
        private string _createby;
        private DateTime? _loginon;
        private string _logininfo;
        private string _loginip;
        /// <summary>
        /// 用户ID
        /// </summary>
        public string USERID
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 用户帐号
        /// </summary>
        public string USERACCOUNT
        {
            set { _useraccount = value; }
            get { return _useraccount; }
        }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string USERPWD
        {
            set { _userpwd = value; }
            get { return _userpwd; }
        }
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string USERNAME
        {
            set { _username = value; }
            get { return _username; }
        }
        /// <summary>
        /// 用户性别
        /// </summary>
        public string USERGENDER
        {
            set { _usergender = value; }
            get { return _usergender; }
        }
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string USEREMAIL
        {
            set { _useremail = value; }
            get { return _useremail; }
        }
        /// <summary>
        /// 手机号
        /// </summary>
        public string USERPHONENUMBER
        {
            set { _userphonenumber = value; }
            get { return _userphonenumber; }
        }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string USERIDNUMBER
        {
            set { _useridnumber = value; }
            get { return _useridnumber; }
        }
        /// <summary>
        /// 用户生日
        /// </summary>
        public DateTime? USERBIRTHDAY
        {
            set { _userbirthday = value; }
            get { return _userbirthday; }
        }
        /// <summary>
        /// 用户照片
        /// </summary>
        public string USERPHOTO
        {
            set { _userphoto = value; }
            get { return _userphoto; }
        }
        /// <summary>
        /// 注销判别
        /// </summary>
        public bool ISDELETED
        {
            set { _isdeleted = value; }
            get { return _isdeleted; }
        }
        /// <summary>
        /// 有效帐号
        /// </summary>
        public bool ISVALID
        {
            set { _isvalid = value; }
            get { return _isvalid; }
        }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime? CREATEON
        {
            set { _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// 建立人
        /// </summary>
        public string CREATEBY
        {
            set { _createby = value; }
            get { return _createby; }
        }
        /// <summary>
        /// 最近登录时间
        /// </summary>
        public DateTime? LOGINON
        {
            set { _loginon = value; }
            get { return _loginon; }
        }
        /// <summary>
        /// 最近登录信息
        /// </summary>
        public string LOGININFO
        {
            set { _logininfo = value; }
            get { return _logininfo; }
        }
        /// <summary>
        /// 最近登录IP
        /// </summary>
        public string LOGINIP
        {
            set { _loginip = value; }
            get { return _loginip; }
        }
    }
}