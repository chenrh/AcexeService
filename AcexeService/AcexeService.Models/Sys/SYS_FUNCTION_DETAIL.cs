﻿//
// SYS_FUNCTION_DETAIL.cs
// 
// shadowee@qq.com @2017/2/6 15:24:46

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统功能配置明细表
	/// </summary>
	[Serializable]
	public partial class SYS_FUNCTION_DETAIL
	{
		public SYS_FUNCTION_DETAIL()
		{}
		#region Model
		private string _funcdetailid;
		private string _funcid;
		private string _customcolumn;
		private string _customcolname;
		private string _customcoltype;
		private string _customcolinputtype;
		private string _customcolinputval;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 明细id
		/// </summary>
		public string FUNCDETAILID
		{
			set{ _funcdetailid=value;}
			get{return _funcdetailid;}
		}
		/// <summary>
		/// 功能id
		/// </summary>
		public string FUNCID
		{
			set{ _funcid=value;}
			get{return _funcid;}
		}
		/// <summary>
		/// 字段名称
		/// </summary>
		public string CUSTOMCOLUMN
		{
			set{ _customcolumn=value;}
			get{return _customcolumn;}
		}
		/// <summary>
		/// 字段中文名称
		/// </summary>
		public string CUSTOMCOLNAME
		{
			set{ _customcolname=value;}
			get{return _customcolname;}
		}
		/// <summary>
		/// 字段类型
		/// </summary>
		public string CUSTOMCOLTYPE
		{
			set{ _customcoltype=value;}
			get{return _customcoltype;}
		}
		/// <summary>
		/// 字段输入类型
		/// </summary>
		public string CUSTOMCOLINPUTTYPE
		{
			set{ _customcolinputtype=value;}
			get{return _customcolinputtype;}
		}
		/// <summary>
		/// 字段输入值
		/// </summary>
		public string CUSTOMCOLINPUTVAL
		{
			set{ _customcolinputval=value;}
			get{return _customcolinputval;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

