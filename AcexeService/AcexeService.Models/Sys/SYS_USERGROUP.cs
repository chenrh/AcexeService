﻿using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 类SYS_USERGROUP。
	/// </summary>
	[Serializable]
	public partial class SYS_USERGROUP
	{
	 
		private string _groupid;
		private string _groupname;
		private bool _isdeleted= false;
		/// <summary>
		/// 用户组id
		/// </summary>
		public string GROUPID
		{
			set{ _groupid=value;}
			get{return _groupid;}
		}
		/// <summary>
		/// 用户组名称
		/// </summary>
		public string GROUPNAME
		{
			set{ _groupname=value;}
			get{return _groupname;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
  
	}
}

