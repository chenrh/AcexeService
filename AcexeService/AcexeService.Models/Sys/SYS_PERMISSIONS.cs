﻿//
// SYS_PERMISSIONS.cs
// 
// shadowee@qq.com @2017/2/6 15:24:48

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统菜单权限表
	/// </summary>
	[Serializable]
	public partial class SYS_PERMISSIONS
	{
		public SYS_PERMISSIONS()
		{}
		#region Model
		private string _permissionid;
		private string _permissionname;
		/// <summary>
		/// 权限id
		/// </summary>
		public string PERMISSIONID
		{
			set{ _permissionid=value;}
			get{return _permissionid;}
		}
		/// <summary>
		/// 权限名称
		/// </summary>
		public string PERMISSIONNAME
		{
			set{ _permissionname=value;}
			get{return _permissionname;}
		}
		#endregion Model

	}
}

