﻿//
// SYS_MENU.cs
// 
// shadowee@qq.com @2017/2/6 15:24:46

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统菜单表
	/// </summary>
	[Serializable]
	public partial class SYS_MENU
	{
		public SYS_MENU()
		{}
		#region Model
		private string _menuid;
		private string _funcid;
		private string _menuname;
		private string _menuparam;
		private string _menustyle;
		private int _menuordernum=1;
		private string _menuparentid;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 菜单id
		/// </summary>
		public string MENUID
		{
			set{ _menuid=value;}
			get{return _menuid;}
		}
		/// <summary>
		/// 功能id
		/// </summary>
		public string FUNCID
		{
			set{ _funcid=value;}
			get{return _funcid;}
		}
		/// <summary>
		/// 菜单名称
		/// </summary>
		public string MENUNAME
		{
			set{ _menuname=value;}
			get{return _menuname;}
		}
		/// <summary>
		/// 菜单参数
		/// </summary>
		public string MENUPARAM
		{
			set{ _menuparam=value;}
			get{return _menuparam;}
		}
		/// <summary>
		/// 菜单样式
		/// </summary>
		public string MENUSTYLE
		{
			set{ _menustyle=value;}
			get{return _menustyle;}
		}
		/// <summary>
		/// 菜单排序号
		/// </summary>
		public int MENUORDERNUM
		{
			set{ _menuordernum=value;}
			get{return _menuordernum;}
		}
		/// <summary>
		/// 菜单上级id
		/// </summary>
		public string MENUPARENTID
		{
			set{ _menuparentid=value;}
			get{return _menuparentid;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

