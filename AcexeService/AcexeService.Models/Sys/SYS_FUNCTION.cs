﻿//
// SYS_FUNCTION.cs
// 
// shadowee@qq.com @2017/2/6 15:24:45

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统功能管理表
	/// </summary>
	[Serializable]
	public partial class SYS_FUNCTION
	{
		public SYS_FUNCTION()
		{}
		#region Model
		private string _funcid;
		private string _funcname;
		private string _funcdisplayname;
		private string _funcdesc;
		private string _funcurl;
		private string _funcdocurl;
		private string _functype;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 功能id
		/// </summary>
		public string FUNCID
		{
			set{ _funcid=value;}
			get{return _funcid;}
		}
		/// <summary>
		/// 功能名称
		/// </summary>
		public string FUNCNAME
		{
			set{ _funcname=value;}
			get{return _funcname;}
		}
		/// <summary>
		/// 功能显示名称
		/// </summary>
		public string FUNCDISPLAYNAME
		{
			set{ _funcdisplayname=value;}
			get{return _funcdisplayname;}
		}
		/// <summary>
		/// 功能介绍
		/// </summary>
		public string FUNCDESC
		{
			set{ _funcdesc=value;}
			get{return _funcdesc;}
		}
		/// <summary>
		/// 功能URL
		/// </summary>
		public string FUNCURL
		{
			set{ _funcurl=value;}
			get{return _funcurl;}
		}
		/// <summary>
		/// 功能文档URL
		/// </summary>
		public string FUNCDOCURL
		{
			set{ _funcdocurl=value;}
			get{return _funcdocurl;}
		}
		/// <summary>
		/// 功能类型
		/// </summary>
		public string FUNCTYPE
		{
			set{ _functype=value;}
			get{return _functype;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

