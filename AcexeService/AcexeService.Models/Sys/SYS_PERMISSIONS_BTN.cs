﻿//
// SYS_PERMISSIONS_BTN.cs
// 
// shadowee@qq.com @2017/2/6 15:24:49

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统菜单权限表
	/// </summary>
	[Serializable]
	public partial class SYS_PERMISSIONS_BTN
	{
		public SYS_PERMISSIONS_BTN()
		{}
		#region Model
		private string _buttonid;
		private string _permissionid;
		private string _objectid;
		private string _perremark;
		/// <summary>
		/// 按钮id
		/// </summary>
		public string BUTTONID
		{
			set{ _buttonid=value;}
			get{return _buttonid;}
		}
		/// <summary>
		/// 权限id
		/// </summary>
		public string PERMISSIONID
		{
			set{ _permissionid=value;}
			get{return _permissionid;}
		}
		/// <summary>
		/// 功能或报表id
		/// </summary>
		public string OBJECTID
		{
			set{ _objectid=value;}
			get{return _objectid;}
		}
		/// <summary>
		/// 权限说明
		/// </summary>
		public string PERREMARK
		{
			set{ _perremark=value;}
			get{return _perremark;}
		}
		#endregion Model

	}
}

