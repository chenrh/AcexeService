﻿//
// SYS_PERMISSIONS_ORG.cs
// 
// shadowee@qq.com @2017/2/27 14:29:43

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统权限组织对应表
	/// </summary>
	[Serializable]
	public partial class SYS_PERMISSIONS_ORG
	{
		public SYS_PERMISSIONS_ORG()
		{}
		#region Model
		private string _permissionid;
		private string _orgid;
		/// <summary>
		/// 权限id
		/// </summary>
		public string PERMISSIONID
		{
			set{ _permissionid=value;}
			get{return _permissionid;}
		}
		/// <summary>
		/// 组织id
		/// </summary>
		public string ORGID
		{
			set{ _orgid=value;}
			get{return _orgid;}
		}
		#endregion Model

	}
}

