﻿//
// SYS_PARAMETER.cs
// 
// shadowee@qq.com @2017/2/6 15:24:47

using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 系统参数表
    /// </summary>
    [Serializable]
    public partial class SYS_PARAMETER
    {
        public SYS_PARAMETER()
        { }
        #region Model
        private string _paramid;
        private string _paramno;
        private string _paramname;
        private string _paramcateid;
        private string _paramdesc;
        private string _paramdefaultval;
        private string _paramval;
        private string _paramvaldesc;
        private string _paramremark;
        private bool _isdeleted = false;
        private DateTime? _updateon;
        private string _updateby;
        private DateTime? _createon;
        private string _createby;
        /// <summary>
        /// 参数id
        /// </summary>
        public string PARAMID
        {
            set { _paramid = value; }
            get { return _paramid; }
        }
        /// <summary>
        /// 参数代码
        /// </summary>
        public string PARAMNO
        {
            set { _paramno = value; }
            get { return _paramno; }
        }
        /// <summary>
        /// 参数名称
        /// </summary>
        public string PARAMNAME
        {
            set { _paramname = value; }
            get { return _paramname; }
        }
        /// <summary>
        /// 参数类别
        /// </summary>
        public string PARAMCATEID
        {
            set { _paramcateid = value; }
            get { return _paramcateid; }
        }
        /// <summary>
        /// 参数描述
        /// </summary>
        public string PARAMDESC
        {
            set { _paramdesc = value; }
            get { return _paramdesc; }
        }
        /// <summary>
        /// 参数初始值
        /// </summary>
        public string PARAMDEFAULTVAL
        {
            set { _paramdefaultval = value; }
            get { return _paramdefaultval; }
        }
        /// <summary>
        /// 参数值
        /// </summary>
        public string PARAMVAL
        {
            set { _paramval = value; }
            get { return _paramval; }
        }
        /// <summary>
        /// 参数值说明
        /// </summary>
        public string PARAMVALDESC
        {
            set { _paramvaldesc = value; }
            get { return _paramvaldesc; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string PARAMREMARK
        {
            set { _paramremark = value; }
            get { return _paramremark; }
        }
        /// <summary>
        /// 作废判别
        /// </summary>
        public bool ISDELETED
        {
            set { _isdeleted = value; }
            get { return _isdeleted; }
        }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? UPDATEON
        {
            set { _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UPDATEBY
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime? CREATEON
        {
            set { _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// 建立人
        /// </summary>
        public string CREATEBY
        {
            set { _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
}

