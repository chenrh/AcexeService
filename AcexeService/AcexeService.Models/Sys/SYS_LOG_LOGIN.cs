﻿//
// SYS_LOG_LOGIN.cs
// 
// shadowee@qq.com @2017/2/8 17:55:20
using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 系统登录日志
    /// </summary>
    [Serializable]
    public partial class SYS_LOG_LOGIN
    {
        public SYS_LOG_LOGIN()
        { }
        #region Model
        private string _logid;
        private string _uesrid;
        private string _loginaccount;
        private DateTime? _loginon;
        private string _loginip;
        private string _loginremark;
        private string _loginmessage;
        private bool _loginsuccess;
        /// <summary>
        /// 主键ID
        /// </summary>
        public string LOGID
        {
            set { _logid = value; }
            get { return _logid; }
        }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string UESRID
        {
            set { _uesrid = value; }
            get { return _uesrid; }
        }
        /// <summary>
        /// 登录帐号
        /// </summary>
        public string LOGINACCOUNT
        {
            set { _loginaccount = value; }
            get { return _loginaccount; }
        }
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime? LOGINON
        {
            set { _loginon = value; }
            get { return _loginon; }
        }
        /// <summary>
        /// 登录IP
        /// </summary>
        public string LOGINIP
        {
            set { _loginip = value; }
            get { return _loginip; }
        }
        /// <summary>
        /// 登录详细
        /// </summary>
        public string LOGINREMARK
        {
            set { _loginremark = value; }
            get { return _loginremark; }
        }
        /// <summary>
        /// 登录消息
        /// </summary>
        public string LOGINMESSAGE
        {
            set { _loginmessage = value; }
            get { return _loginmessage; }
        }
        /// <summary>
        /// 登录成功
        /// </summary>
        public bool LOGINSUCCESS
        {
            set { _loginsuccess = value; }
            get { return _loginsuccess; }
        }
        #endregion Model

    }
}