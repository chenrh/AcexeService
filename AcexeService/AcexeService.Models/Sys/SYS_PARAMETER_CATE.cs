﻿//
// SYS_PARAMETER_CATE.cs
// 
// shadowee@qq.com @2017/2/8 14:10:44
using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 系统参数类别表
    /// </summary>
    [Serializable]
    public partial class SYS_PARAMETER_CATE
    {
        public SYS_PARAMETER_CATE()
        { }
        #region Model
        private string _paramcateid;
        private string _paramcatename;
        private int? _paramcateorder;
        private string _paramcateparentid;
        private string _paramremark;
        private bool _isdeleted = false;
        private DateTime? _updateon;
        private string _updateby;
        private DateTime? _createon;
        private string _createby;
        /// <summary>
        /// 参数类别id
        /// </summary>
        public string PARAMCATEID
        {
            set { _paramcateid = value; }
            get { return _paramcateid; }
        }
        /// <summary>
        /// 参数类别名称
        /// </summary>
        public string PARAMCATENAME
        {
            set { _paramcatename = value; }
            get { return _paramcatename; }
        }
        /// <summary>
        /// 排序号
        /// </summary>
        public int? PARAMCATEORDER
        {
            set { _paramcateorder = value; }
            get { return _paramcateorder; }
        }
        /// <summary>
        /// 父类别id
        /// </summary>
        public string PARAMCATEPARENTID
        {
            set { _paramcateparentid = value; }
            get { return _paramcateparentid; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string PARAMREMARK
        {
            set { _paramremark = value; }
            get { return _paramremark; }
        }
        /// <summary>
        /// 作废判别
        /// </summary>
        public bool ISDELETED
        {
            set { _isdeleted = value; }
            get { return _isdeleted; }
        }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? UPDATEON
        {
            set { _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UPDATEBY
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime? CREATEON
        {
            set { _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// 建立人
        /// </summary>
        public string CREATEBY
        {
            set { _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
}

