﻿//
// SYS_PERMISSIONS_USERGROUP.cs
// 
// shadowee@qq.com @2017/2/6 15:24:50

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统权限用户组对应表
	/// </summary>
	[Serializable]
	public partial class SYS_PERMISSIONS_USERGROUP
	{
		public SYS_PERMISSIONS_USERGROUP()
		{}
		#region Model
		private string _permissionid;
		private string _groupid;
		/// <summary>
		/// 权限id
		/// </summary>
		public string PERMISSIONID
		{
			set{ _permissionid=value;}
			get{return _permissionid;}
		}
		/// <summary>
		/// 用户组id
		/// </summary>
		public string GROUPID
		{
			set{ _groupid=value;}
			get{return _groupid;}
		}
		#endregion Model

	}
}

