﻿//
// SYS_MODULE.cs
// 
// shadowee@qq.com @2017/2/9 11:41:21

using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 系统模块
    /// </summary>
    [Serializable]
    public partial class SYS_MODULE
    {
        public SYS_MODULE()
        { }
        #region Model
        private string _moduleid;
        private string _modulename;
        private string _moduleplatform = "BS";
        private bool _isdeleted = false;
        private int _ordernum = 1;
        /// <summary>
        /// 模块ID
        /// </summary>
        public string MODULEID
        {
            set { _moduleid = value; }
            get { return _moduleid; }
        }
        /// <summary>
        /// 模块名称
        /// </summary>
        public string MODULENAME
        {
            set { _modulename = value; }
            get { return _modulename; }
        }
        /// <summary>
        /// 模块平台
        /// </summary>
        public string MODULEPLATFORM
        {
            set { _moduleplatform = value; }
            get { return _moduleplatform; }
        }
        /// <summary>
        /// 作废判别
        /// </summary>
        public bool ISDELETED
        {
            set { _isdeleted = value; }
            get { return _isdeleted; }
        }
        /// <summary>
        /// 排序号
        /// </summary>
        public int ORDERNUM
        {
            set { _ordernum = value; }
            get { return _ordernum; }
        }
        #endregion Model

    }
}

