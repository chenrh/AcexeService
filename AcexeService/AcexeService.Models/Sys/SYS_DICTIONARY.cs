﻿using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 系统数据字典
    /// </summary>
    [Serializable]
    public partial class SYS_DICTIONARY
    {
        public SYS_DICTIONARY()
        { }
        #region Model
        private string _dicid;
        private string _dicname;
        private string _dicremark;
        private bool _isdeleted = false;
        private int _ordernum = 1;
        private DateTime? _updateon;
        private string _updateby;
        private DateTime? _createon;
        private string _createby;
        /// <summary>
        /// 字典代码
        /// </summary>
        public string DICID
        {
            set { _dicid = value; }
            get { return _dicid; }
        }
        /// <summary>
        /// 字典名称
        /// </summary>
        public string DICNAME
        {
            set { _dicname = value; }
            get { return _dicname; }
        }
        /// <summary>
        /// 字典说明
        /// </summary>
        public string DICREMARK
        {
            set { _dicremark = value; }
            get { return _dicremark; }
        }
        /// <summary>
        /// 作废判别
        /// </summary>
        public bool ISDELETED
        {
            set { _isdeleted = value; }
            get { return _isdeleted; }
        }
        /// <summary>
        /// 排序号
        /// </summary>
        public int ORDERNUM
        {
            set { _ordernum = value; }
            get { return _ordernum; }
        }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? UPDATEON
        {
            set { _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UPDATEBY
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime? CREATEON
        {
            set { _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// 建立人
        /// </summary>
        public string CREATEBY
        {
            set { _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
}

