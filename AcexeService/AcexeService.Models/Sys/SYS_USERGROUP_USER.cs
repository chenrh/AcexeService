﻿using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 类SYS_USERGROUP_USER。
    /// </summary>
    [Serializable]
    public partial class SYS_USERGROUP_USER
    {
        private string _groupid;
        private string _userid;
        /// <summary>
        /// 用户组id
        /// </summary>
        public string GROUPID
        {
            set { _groupid = value; }
            get { return _groupid; }
        }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string USERID
        {
            set { _userid = value; }
            get { return _userid; }
        }

    }
}

