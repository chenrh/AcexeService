﻿//
// SYS_PERMISSIONS_FUNC.cs
// 
// shadowee@qq.com @2017/2/6 15:24:49

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统菜单权限表
	/// </summary>
	[Serializable]
	public partial class SYS_PERMISSIONS_FUNC
	{
		public SYS_PERMISSIONS_FUNC()
		{}
		#region Model
        private string _funcid;
		private string _permissionid;
		/// <summary>
		/// 功能id
		/// </summary>
		public string FUNCID
		{
            set { _funcid = value; }
            get { return _funcid; }
		}

		/// <summary>
		/// 权限id
		/// </summary>
		public string PERMISSIONID
		{
			set{ _permissionid=value;}
			get{return _permissionid;}
		}
		#endregion Model

	}
}

