﻿//
// SYS_BUTTON.cs
// 
// shadowee@qq.com @2017/2/6 15:24:44

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 系统按钮
	/// </summary>
	[Serializable]
	public partial class SYS_BUTTON
	{
		public SYS_BUTTON()
		{}
		#region Model
		private string _buttonid;
		private string _buttonremark;
		/// <summary>
		/// 按钮id
		/// </summary>
		public string BUTTONID
		{
			set{ _buttonid=value;}
			get{return _buttonid;}
		}
		/// <summary>
		/// 按钮说明
		/// </summary>
		public string BUTTONREMARK
		{
			set{ _buttonremark=value;}
			get{return _buttonremark;}
		}
		#endregion Model

	}
}

