﻿//
// SYS_MODULE_MENU.cs
// 
// shadowee@qq.com @2017/2/9 11:50:21

using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 系统模块菜单
    /// </summary>
    [Serializable]
    public partial class SYS_MODULE_MENU
    {
        public SYS_MODULE_MENU()
        { }
        #region Model
        private string _moduleid;
        private string _menuid;
        /// <summary>
        /// 模块ID
        /// </summary>
        public string MODULEID
        {
            set { _moduleid = value; }
            get { return _moduleid; }
        }
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string MENUID
        {
            set { _menuid = value; }
            get { return _menuid; }
        }
        #endregion Model

    }
}

