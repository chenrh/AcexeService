﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.Models
{
    public class DictConfig
    {
        /// <summary>
        /// 组组架构类型 字典代码
        /// </summary>
        public const string ORGANIZATION_CATEGORY_DICID = "10002";
    }
}
