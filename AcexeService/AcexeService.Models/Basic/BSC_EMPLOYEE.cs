﻿//
// BSC_EMPLOYEE.cs
// 
// shadowee@qq.com @2017/2/23 10:30:04

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 员工表
	/// </summary>
	[Serializable]
	public partial class BSC_EMPLOYEE
	{
		public BSC_EMPLOYEE()
		{}
		#region Model
		private string _empid;
		private string _empno;
		private string _empname;
		private string _emptel;
		private string _emptel2;
		private string _empfamily;
		private string _empfamilyrelation;
		private string _empfamilytel;
		private string _empfamilyaddr;
		private string _empnation;
		private string _empnatvieplace;
		private string _empgender;
		private string _empeducation;
		private string _empphoto;
		private string _empselfintroduction;
		private string _empuserid;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 员工id
		/// </summary>
		public string EMPID
		{
			set{ _empid=value;}
			get{return _empid;}
		}
		/// <summary>
		/// 员工编号
		/// </summary>
		public string EMPNO
		{
			set{ _empno=value;}
			get{return _empno;}
		}
		/// <summary>
		/// 员工姓名
		/// </summary>
		public string EMPNAME
		{
			set{ _empname=value;}
			get{return _empname;}
		}
		/// <summary>
		/// 联系手机
		/// </summary>
		public string EMPTEL
		{
			set{ _emptel=value;}
			get{return _emptel;}
		}
		/// <summary>
		/// 联系短号
		/// </summary>
		public string EMPTEL2
		{
			set{ _emptel2=value;}
			get{return _emptel2;}
		}
		/// <summary>
		/// 家庭联系人
		/// </summary>
		public string EMPFAMILY
		{
			set{ _empfamily=value;}
			get{return _empfamily;}
		}
		/// <summary>
		/// 与员工关系
		/// </summary>
		public string EMPFAMILYRELATION
		{
			set{ _empfamilyrelation=value;}
			get{return _empfamilyrelation;}
		}
		/// <summary>
		/// 家庭联系人电话
		/// </summary>
		public string EMPFAMILYTEL
		{
			set{ _empfamilytel=value;}
			get{return _empfamilytel;}
		}
		/// <summary>
		/// 家庭住址
		/// </summary>
		public string EMPFAMILYADDR
		{
			set{ _empfamilyaddr=value;}
			get{return _empfamilyaddr;}
		}
		/// <summary>
		/// 民族
		/// </summary>
		public string EMPNATION
		{
			set{ _empnation=value;}
			get{return _empnation;}
		}
		/// <summary>
		/// 籍贯
		/// </summary>
		public string EMPNATVIEPLACE
		{
			set{ _empnatvieplace=value;}
			get{return _empnatvieplace;}
		}
		/// <summary>
		/// 性别
		/// </summary>
		public string EMPGENDER
		{
			set{ _empgender=value;}
			get{return _empgender;}
		}
		/// <summary>
		/// 最高学历
		/// </summary>
		public string EMPEDUCATION
		{
			set{ _empeducation=value;}
			get{return _empeducation;}
		}
		/// <summary>
		/// 员工照片
		/// </summary>
		public string EMPPHOTO
		{
			set{ _empphoto=value;}
			get{return _empphoto;}
		}
		/// <summary>
		/// 员工自我介绍
		/// </summary>
		public string EMPSELFINTRODUCTION
		{
			set{ _empselfintroduction=value;}
			get{return _empselfintroduction;}
		}
		/// <summary>
		/// 对应用户ID
		/// </summary>
		public string EMPUSERID
		{
			set{ _empuserid=value;}
			get{return _empuserid;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

