﻿//
// BSC_JOBNAME.cs
// 
// shadowee@qq.com @2017/2/23 10:30:05

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 岗位名称表
	/// </summary>
	[Serializable]
	public partial class BSC_JOBNAME
	{
		public BSC_JOBNAME()
		{}
		#region Model
		private string _jobid;
		private string _jobno;
		private string _jobname;
		private decimal? _jobworkhours;
		private decimal? _jobavgworkhours;
		private int _ordernum;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 岗位id
		/// </summary>
		public string JOBID
		{
			set{ _jobid=value;}
			get{return _jobid;}
		}
		/// <summary>
		/// 岗位编号
		/// </summary>
		public string JOBNO
		{
			set{ _jobno=value;}
			get{return _jobno;}
		}
		/// <summary>
		/// 岗位名称
		/// </summary>
		public string JOBNAME
		{
			set{ _jobname=value;}
			get{return _jobname;}
		}
		/// <summary>
		/// 岗位工时
		/// </summary>
		public decimal? JOBWORKHOURS
		{
			set{ _jobworkhours=value;}
			get{return _jobworkhours;}
		}
		/// <summary>
		/// 岗位平均工时
		/// </summary>
		public decimal? JOBAVGWORKHOURS
		{
			set{ _jobavgworkhours=value;}
			get{return _jobavgworkhours;}
		}
		/// <summary>
		/// 排序号
		/// </summary>
		public int ORDERNUM
		{
			set{ _ordernum=value;}
			get{return _ordernum;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

