﻿//
// BSC_ORG_JOB.cs
// 
// shadowee@qq.com @2017/2/23 10:30:05

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 岗位名称表
	/// </summary>
	[Serializable]
	public partial class BSC_ORG_JOB
	{
		public BSC_ORG_JOB()
		{}
		#region Model
		private string _jobid;
		private string _orgid;
		/// <summary>
		/// 岗位id
		/// </summary>
		public string JOBID
		{
			set{ _jobid=value;}
			get{return _jobid;}
		}
		/// <summary>
		/// 门部id
		/// </summary>
		public string ORGID
		{
			set{ _orgid=value;}
			get{return _orgid;}
		}
		#endregion Model

	}
}

