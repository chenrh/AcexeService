﻿//
// PRO_PRODUCT_CATE.cs
// 
// shadowee@qq.com @2017/2/17 15:25:03

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品分类表
	/// </summary>
	[Serializable]
	public partial class PRO_PRODUCT_CATE
	{
		public PRO_PRODUCT_CATE()
		{}
		#region Model
		private string _procateid;
		private string _procatename;
		private int? _ordernum;
		private string _procateparentid;
		private string _paramremark;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 商品类别id
		/// </summary>
		public string PROCATEID
		{
			set{ _procateid=value;}
			get{return _procateid;}
		}
		/// <summary>
		/// 类别名称
		/// </summary>
		public string PROCATENAME
		{
			set{ _procatename=value;}
			get{return _procatename;}
		}
		/// <summary>
		/// 排序号
		/// </summary>
		public int? ORDERNUM
		{
			set{ _ordernum=value;}
			get{return _ordernum;}
		}
		/// <summary>
		/// 父类别id
		/// </summary>
		public string PROCATEPARENTID
		{
			set{ _procateparentid=value;}
			get{return _procateparentid;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string PARAMREMARK
		{
			set{ _paramremark=value;}
			get{return _paramremark;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

