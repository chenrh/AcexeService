﻿//
// PRO_PROID_PROCATEID.cs
// 
// shadowee@qq.com @2017/2/17 15:25:05

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品与商品分类对应表
	/// </summary>
	[Serializable]
	public partial class PRO_PROID_PROCATEID
	{
		public PRO_PROID_PROCATEID()
		{}
		#region Model
		private string _procateid;
		private string _proid;
		/// <summary>
		/// 商品类别id
		/// </summary>
		public string PROCATEID
		{
			set{ _procateid=value;}
			get{return _procateid;}
		}
		/// <summary>
		/// 商品id
		/// </summary>
		public string PROID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		#endregion Model

	}
}

