﻿//
// PRO_PRODUCT_PIC.cs
// 
// shadowee@qq.com @2017/2/17 15:25:04

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品图片表
	/// </summary>
	[Serializable]
	public partial class PRO_PRODUCT_PIC
	{
		public PRO_PRODUCT_PIC()
		{}
		#region Model
		private string _picid;
		private string _proid;
		private string _picsrc;
		private string _pictitle;
		private string _picstyle;
		private string _picremark;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 主键id
		/// </summary>
		public string PICID
		{
			set{ _picid=value;}
			get{return _picid;}
		}
		/// <summary>
		/// 商品id
		/// </summary>
		public string PROID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 图片地址
		/// </summary>
		public string PICSRC
		{
			set{ _picsrc=value;}
			get{return _picsrc;}
		}
		/// <summary>
		/// 图片标题
		/// </summary>
		public string PICTITLE
		{
			set{ _pictitle=value;}
			get{return _pictitle;}
		}
		/// <summary>
		/// 图片显示样式
		/// </summary>
		public string PICSTYLE
		{
			set{ _picstyle=value;}
			get{return _picstyle;}
		}
		/// <summary>
		/// 图片备注
		/// </summary>
		public string PICREMARK
		{
			set{ _picremark=value;}
			get{return _picremark;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

