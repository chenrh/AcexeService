﻿//
// PRO_PRODUCT.cs
// 
// shadowee@qq.com @2017/2/17 15:25:03

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品资料表
	/// </summary>
	[Serializable]
	public partial class PRO_PRODUCT
	{
		public PRO_PRODUCT()
		{}
		#region Model
		private string _proid;
		private string _proname;
		private string _proaliasname;
		private string _proenname;
		private string _proshortnum;
		private string _prooriginalnum;
		private string _prounitname;
		private string _protype;
		private string _prospecifications;
		private string _proremark;
		private bool _isdeleted= false;
		private int _ordernum=1;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 商品ID
		/// </summary>
		public string PROID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 商品名称
		/// </summary>
		public string PRONAME
		{
			set{ _proname=value;}
			get{return _proname;}
		}
		/// <summary>
		/// 商品别名
		/// </summary>
		public string PROALIASNAME
		{
			set{ _proaliasname=value;}
			get{return _proaliasname;}
		}
		/// <summary>
		/// 商品英文名
		/// </summary>
		public string PROENNAME
		{
			set{ _proenname=value;}
			get{return _proenname;}
		}
		/// <summary>
		/// 商品短号
		/// </summary>
		public string PROSHORTNUM
		{
			set{ _proshortnum=value;}
			get{return _proshortnum;}
		}
		/// <summary>
		/// 商品原厂编号
		/// </summary>
		public string PROORIGINALNUM
		{
			set{ _prooriginalnum=value;}
			get{return _prooriginalnum;}
		}
		/// <summary>
		/// 商品单位
		/// </summary>
		public string PROUNITNAME
		{
			set{ _prounitname=value;}
			get{return _prounitname;}
		}
		/// <summary>
		/// 商品型号
		/// </summary>
		public string PROTYPE
		{
			set{ _protype=value;}
			get{return _protype;}
		}
		/// <summary>
		/// 商品规格
		/// </summary>
		public string PROSPECIFICATIONS
		{
			set{ _prospecifications=value;}
			get{return _prospecifications;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string PROREMARK
		{
			set{ _proremark=value;}
			get{return _proremark;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 排序号
		/// </summary>
		public int ORDERNUM
		{
			set{ _ordernum=value;}
			get{return _ordernum;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

