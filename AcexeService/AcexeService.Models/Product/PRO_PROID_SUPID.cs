﻿//
// PRO_PROID_SUPID.cs
// 
// shadowee@qq.com @2017/2/17 15:25:05

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品与商品供应商对应表
	/// </summary>
	[Serializable]
	public partial class PRO_PROID_SUPID
	{
		public PRO_PROID_SUPID()
		{}
		#region Model
		private string _supid;
		private string _proid;
		private bool _supvalid= true;
		/// <summary>
		/// 供应商id
		/// </summary>
		public string SUPID
		{
			set{ _supid=value;}
			get{return _supid;}
		}
		/// <summary>
		/// 商品id
		/// </summary>
		public string PROID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 是否启用
		/// </summary>
		public bool SUPVALID
		{
			set{ _supvalid=value;}
			get{return _supvalid;}
		}
		#endregion Model

	}
}

