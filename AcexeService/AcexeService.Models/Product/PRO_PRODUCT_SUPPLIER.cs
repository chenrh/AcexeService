﻿//
// PRO_PRODUCT_SUPPLIER.cs
// 
// shadowee@qq.com @2017/2/17 15:25:04

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品供应商表
	/// </summary>
	[Serializable]
	public partial class PRO_PRODUCT_SUPPLIER
	{
		public PRO_PRODUCT_SUPPLIER()
		{}
		#region Model
		private string _supid;
		private string _supname;
		private string _supaliasname;
		private string _suppinyin;
		private string _supaddr;
		private string _suptel;
		private string _supcontacter;
		private int? _suplevel;
		private string _supcontactertel;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 供应商id
		/// </summary>
		public string SUPID
		{
			set{ _supid=value;}
			get{return _supid;}
		}
		/// <summary>
		/// 供应商名称
		/// </summary>
		public string SUPNAME
		{
			set{ _supname=value;}
			get{return _supname;}
		}
		/// <summary>
		/// 供应商别名
		/// </summary>
		public string SUPALIASNAME
		{
			set{ _supaliasname=value;}
			get{return _supaliasname;}
		}
		/// <summary>
		/// 首拼简写
		/// </summary>
		public string SUPPINYIN
		{
			set{ _suppinyin=value;}
			get{return _suppinyin;}
		}
		/// <summary>
		/// 供应商地址
		/// </summary>
		public string SUPADDR
		{
			set{ _supaddr=value;}
			get{return _supaddr;}
		}
		/// <summary>
		/// 供应商电话
		/// </summary>
		public string SUPTEL
		{
			set{ _suptel=value;}
			get{return _suptel;}
		}
		/// <summary>
		/// 供应商联系人
		/// </summary>
		public string SUPCONTACTER
		{
			set{ _supcontacter=value;}
			get{return _supcontacter;}
		}
		/// <summary>
		/// 优先级
		/// </summary>
		public int? SUPLEVEL
		{
			set{ _suplevel=value;}
			get{return _suplevel;}
		}
		/// <summary>
		/// 联系人
		/// </summary>
		public string SUPCONTACTERTEL
		{
			set{ _supcontactertel=value;}
			get{return _supcontactertel;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

