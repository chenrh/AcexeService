﻿//
// PRO_PROID_WAREID.cs
// 
// shadowee@qq.com @2017/2/17 15:25:05

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品与商品仓库对应表
	/// </summary>
	[Serializable]
	public partial class PRO_PROID_WAREID
	{
		public PRO_PROID_WAREID()
		{}
		#region Model
		private string _wareid;
		private string _proid;
		private bool _warevalid= true;
		/// <summary>
		/// 仓库id
		/// </summary>
		public string WAREID
		{
			set{ _wareid=value;}
			get{return _wareid;}
		}
		/// <summary>
		/// 商品id
		/// </summary>
		public string PROID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 是否启用
		/// </summary>
		public bool WAREVALID
		{
			set{ _warevalid=value;}
			get{return _warevalid;}
		}
		#endregion Model

	}
}

