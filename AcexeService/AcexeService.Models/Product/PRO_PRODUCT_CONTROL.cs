﻿//
// PRO_PRODUCT_CONTROL.cs
// 
// shadowee@qq.com @2017/2/17 15:25:03

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 商品控制条件
	/// </summary>
	[Serializable]
	public partial class PRO_PRODUCT_CONTROL
	{
		public PRO_PRODUCT_CONTROL()
		{}
		#region Model
		private string _proid;
		private bool _firstinfirstout= true;
		private bool _expirmanage= true;
		private bool _safestock= true;
		private bool _pick= true;
		/// <summary>
		/// 商品id
		/// </summary>
		public string PROID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 启用先进先出
		/// </summary>
		public bool FIRSTINFIRSTOUT
		{
			set{ _firstinfirstout=value;}
			get{return _firstinfirstout;}
		}
		/// <summary>
		/// 启用效期管理
		/// </summary>
		public bool EXPIRMANAGE
		{
			set{ _expirmanage=value;}
			get{return _expirmanage;}
		}
		/// <summary>
		/// 启用安全库存
		/// </summary>
		public bool SAFESTOCK
		{
			set{ _safestock=value;}
			get{return _safestock;}
		}
		/// <summary>
		/// 参与拣选
		/// </summary>
		public bool PICK
		{
			set{ _pick=value;}
			get{return _pick;}
		}
		#endregion Model

	}
}

