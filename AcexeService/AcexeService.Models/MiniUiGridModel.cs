﻿//
// 为 前台MiniUI Grid 提供符合格式的数据
//
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.Models
{
    public class MiniUiGridModel<T>
    {
        public int total { get; set; }

        public List<T> data { get; set; }
    }
}
