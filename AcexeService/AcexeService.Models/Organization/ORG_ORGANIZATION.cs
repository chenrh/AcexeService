﻿using System;
namespace AcexeService.Models
{
    /// <summary>
    /// 组织表
    /// </summary>
    [Serializable]
    public partial class ORG_ORGANIZATION
    {
        public ORG_ORGANIZATION()
        { }
        #region Model
        private string _orgid;
        private string _orgno;
        private string _orgname;
        private string _orgpid;
        private int _ordernum;
        private string _orgcate;
        private string _contacter;
        private string _contactertel;
        private string _addr;
        private string _syncplatform;
        private string _syncway;
        private bool _isdeleted = false;
        private DateTime? _updateon;
        private string _updateby;
        private DateTime? _createon;
        private string _createby;
        /// <summary>
        /// 组织id
        /// </summary>
        public string ORGID
        {
            set { _orgid = value; }
            get { return _orgid; }
        }
        /// <summary>
        /// 组织编号
        /// </summary>
        public string ORGNO
        {
            set { _orgno = value; }
            get { return _orgno; }
        }
        /// <summary>
        /// 组织名称
        /// </summary>
        public string ORGNAME
        {
            set { _orgname = value; }
            get { return _orgname; }
        }
        /// <summary>
        /// 上级组织名称
        /// </summary>
        public string ORGPID
        {
            set { _orgpid = value; }
            get { return _orgpid; }
        }
        /// <summary>
        /// 排序号
        /// </summary>
        public int ORDERNUM
        {
            set { _ordernum = value; }
            get { return _ordernum; }
        }
        /// <summary>
        /// 组织类别
        /// </summary>
        public string ORGCATE
        {
            set { _orgcate = value; }
            get { return _orgcate; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string CONTACTER
        {
            set { _contacter = value; }
            get { return _contacter; }
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string CONTACTERTEL
        {
            set { _contactertel = value; }
            get { return _contactertel; }
        }
        /// <summary>
        /// 联系地址
        /// </summary>
        public string ADDR
        {
            set { _addr = value; }
            get { return _addr; }
        }
        /// <summary>
        /// 同步平台
        /// </summary>
        public string SYNCPLATFORM
        {
            set { _syncplatform = value; }
            get { return _syncplatform; }
        }
        /// <summary>
        /// 同步方式
        /// </summary>
        public string SYNCWAY
        {
            set { _syncway = value; }
            get { return _syncway; }
        }
        /// <summary>
        /// 作废判别
        /// </summary>
        public bool ISDELETED
        {
            set { _isdeleted = value; }
            get { return _isdeleted; }
        }
        /// <summary>
        /// 修改日期
        /// </summary>
        public DateTime? UPDATEON
        {
            set { _updateon = value; }
            get { return _updateon; }
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UPDATEBY
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime? CREATEON
        {
            set { _createon = value; }
            get { return _createon; }
        }
        /// <summary>
        /// 建立人
        /// </summary>
        public string CREATEBY
        {
            set { _createby = value; }
            get { return _createby; }
        }
        #endregion Model

    }
}

