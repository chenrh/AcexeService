﻿
namespace AcexeService.Models
{
    /// <summary>
    /// 分页属性
    /// </summary>
    public class PageParam
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        /// <summary>
        /// 现在的问题： 字段名含有关键字，导致用排序字符串时报错
        /// 例如: Potential illegal fragment detected: UPDATEON
        /// ,CREATEBY,ISDELETE 等字段做排序统统报错
        /// </summary>
        public string SortField { get; set; }

        // asc/desc
        public string SortOrder { get; set; }

        // 取全部数据
        public bool IsGetWholeData { get; set; }

    }
}
