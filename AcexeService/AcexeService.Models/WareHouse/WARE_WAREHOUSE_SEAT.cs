﻿//
// WARE_WAREHOUSE_SEAT.cs
// 
// shadowee@qq.com @2017/2/17 15:25:07

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 库区表
	/// </summary>
	[Serializable]
	public partial class WARE_WAREHOUSE_SEAT
	{
		public WARE_WAREHOUSE_SEAT()
		{}
		#region Model
		private string _wareid;
		private string _wareno;
		private string _areaid;
		private string _areano;
		private string _seatid;
		private string _rownum;
		private string _levelnum;
		private string _seatnum;
		private string _deepnum;
		private string _widthnum;
		private string _heightnum;
		private string _maxweightnum;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 仓库id
		/// </summary>
		public string WAREID
		{
			set{ _wareid=value;}
			get{return _wareid;}
		}
		/// <summary>
		/// 仓库编号
		/// </summary>
		public string WARENO
		{
			set{ _wareno=value;}
			get{return _wareno;}
		}
		/// <summary>
		/// 库区id
		/// </summary>
		public string AREAID
		{
			set{ _areaid=value;}
			get{return _areaid;}
		}
		/// <summary>
		/// 库区编号
		/// </summary>
		public string AREANO
		{
			set{ _areano=value;}
			get{return _areano;}
		}
		/// <summary>
		/// 库位id
		/// </summary>
		public string SEATID
		{
			set{ _seatid=value;}
			get{return _seatid;}
		}
		/// <summary>
		/// 行号
		/// </summary>
		public string ROWNUM
		{
			set{ _rownum=value;}
			get{return _rownum;}
		}
		/// <summary>
		/// 层号
		/// </summary>
		public string LEVELNUM
		{
			set{ _levelnum=value;}
			get{return _levelnum;}
		}
		/// <summary>
		/// 位号
		/// </summary>
		public string SEATNUM
		{
			set{ _seatnum=value;}
			get{return _seatnum;}
		}
		/// <summary>
		/// 进深
		/// </summary>
		public string DEEPNUM
		{
			set{ _deepnum=value;}
			get{return _deepnum;}
		}
		/// <summary>
		/// 宽度
		/// </summary>
		public string WIDTHNUM
		{
			set{ _widthnum=value;}
			get{return _widthnum;}
		}
		/// <summary>
		/// 高度
		/// </summary>
		public string HEIGHTNUM
		{
			set{ _heightnum=value;}
			get{return _heightnum;}
		}
		/// <summary>
		/// 承重
		/// </summary>
		public string MAXWEIGHTNUM
		{
			set{ _maxweightnum=value;}
			get{return _maxweightnum;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

