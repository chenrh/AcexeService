﻿//
// WARE_WAREHOUSE_AREA.cs
// 
// shadowee@qq.com @2017/2/17 15:25:06

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 库区表
	/// </summary>
	[Serializable]
	public partial class WARE_WAREHOUSE_AREA
	{
		public WARE_WAREHOUSE_AREA()
		{}
		#region Model
		private string _wareid;
		private string _wareno;
		private string _areaid;
		private string _areano;
		private string _areaname;
		private string _areamanager;
		private string _areamanagertel;
		private string _maxtemperature;
		private string _mintemperature;
		private string _storagedevice;
		private string _storagedevicemaxweight;
		private string _storagedevicelevel;
		private bool _wcs= false;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 仓库id
		/// </summary>
		public string WAREID
		{
			set{ _wareid=value;}
			get{return _wareid;}
		}
		/// <summary>
		/// 仓库编号
		/// </summary>
		public string WARENO
		{
			set{ _wareno=value;}
			get{return _wareno;}
		}
		/// <summary>
		/// 库区id
		/// </summary>
		public string AREAID
		{
			set{ _areaid=value;}
			get{return _areaid;}
		}
		/// <summary>
		/// 库区编号
		/// </summary>
		public string AREANO
		{
			set{ _areano=value;}
			get{return _areano;}
		}
		/// <summary>
		/// 库区名称
		/// </summary>
		public string AREANAME
		{
			set{ _areaname=value;}
			get{return _areaname;}
		}
		/// <summary>
		/// 库区负责人
		/// </summary>
		public string AREAMANAGER
		{
			set{ _areamanager=value;}
			get{return _areamanager;}
		}
		/// <summary>
		/// 库区负责人电话
		/// </summary>
		public string AREAMANAGERTEL
		{
			set{ _areamanagertel=value;}
			get{return _areamanagertel;}
		}
		/// <summary>
		/// 最高温度
		/// </summary>
		public string MAXTEMPERATURE
		{
			set{ _maxtemperature=value;}
			get{return _maxtemperature;}
		}
		/// <summary>
		/// 最低温度
		/// </summary>
		public string MINTEMPERATURE
		{
			set{ _mintemperature=value;}
			get{return _mintemperature;}
		}
		/// <summary>
		/// 存储设备
		/// </summary>
		public string STORAGEDEVICE
		{
			set{ _storagedevice=value;}
			get{return _storagedevice;}
		}
		/// <summary>
		/// 存储设备承重
		/// </summary>
		public string STORAGEDEVICEMAXWEIGHT
		{
			set{ _storagedevicemaxweight=value;}
			get{return _storagedevicemaxweight;}
		}
		/// <summary>
		/// 存储设备层数
		/// </summary>
		public string STORAGEDEVICELEVEL
		{
			set{ _storagedevicelevel=value;}
			get{return _storagedevicelevel;}
		}
		/// <summary>
		/// WCS通讯标识
		/// </summary>
		public bool WCS
		{
			set{ _wcs=value;}
			get{return _wcs;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

