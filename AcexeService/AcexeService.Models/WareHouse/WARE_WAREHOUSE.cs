﻿//
// WARE_WAREHOUSE.cs
// 
// shadowee@qq.com @2017/2/17 15:25:06

using System;
namespace AcexeService.Models
{
	/// <summary>
	/// 仓库表
	/// </summary>
	[Serializable]
	public partial class WARE_WAREHOUSE
	{
		public WARE_WAREHOUSE()
		{}
		#region Model
		private string _wareid;
		private string _wareno;
		private string _warename;
		private string _wareaddr;
		private string _wareprovince;
		private string _warecity;
		private string _warecounty;
		private string _warecontacter;
		private string _waretel;
		private DateTime? _warebirthday;
		private bool _isdeleted= false;
		private DateTime? _updateon;
		private string _updateby;
		private DateTime? _createon;
		private string _createby;
		/// <summary>
		/// 仓库id
		/// </summary>
		public string WAREID
		{
			set{ _wareid=value;}
			get{return _wareid;}
		}
		/// <summary>
		/// 仓库编号
		/// </summary>
		public string WARENO
		{
			set{ _wareno=value;}
			get{return _wareno;}
		}
		/// <summary>
		/// 仓库名称
		/// </summary>
		public string WARENAME
		{
			set{ _warename=value;}
			get{return _warename;}
		}
		/// <summary>
		/// 仓库地址
		/// </summary>
		public string WAREADDR
		{
			set{ _wareaddr=value;}
			get{return _wareaddr;}
		}
		/// <summary>
		/// 所在省
		/// </summary>
		public string WAREPROVINCE
		{
			set{ _wareprovince=value;}
			get{return _wareprovince;}
		}
		/// <summary>
		/// 所在市
		/// </summary>
		public string WARECITY
		{
			set{ _warecity=value;}
			get{return _warecity;}
		}
		/// <summary>
		/// 所在县
		/// </summary>
		public string WARECOUNTY
		{
			set{ _warecounty=value;}
			get{return _warecounty;}
		}
		/// <summary>
		/// 仓库联系人
		/// </summary>
		public string WARECONTACTER
		{
			set{ _warecontacter=value;}
			get{return _warecontacter;}
		}
		/// <summary>
		/// 仓库联系电话
		/// </summary>
		public string WARETEL
		{
			set{ _waretel=value;}
			get{return _waretel;}
		}
		/// <summary>
		/// 仓库建立时间
		/// </summary>
		public DateTime? WAREBIRTHDAY
		{
			set{ _warebirthday=value;}
			get{return _warebirthday;}
		}
		/// <summary>
		/// 作废判别
		/// </summary>
		public bool ISDELETED
		{
			set{ _isdeleted=value;}
			get{return _isdeleted;}
		}
		/// <summary>
		/// 修改日期
		/// </summary>
		public DateTime? UPDATEON
		{
			set{ _updateon=value;}
			get{return _updateon;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string UPDATEBY
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 建立日期
		/// </summary>
		public DateTime? CREATEON
		{
			set{ _createon=value;}
			get{return _createon;}
		}
		/// <summary>
		/// 建立人
		/// </summary>
		public string CREATEBY
		{
			set{ _createby=value;}
			get{return _createby;}
		}
		#endregion Model

	}
}

