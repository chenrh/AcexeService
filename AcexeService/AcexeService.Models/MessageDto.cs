﻿//
// 通用消息 DTO 
//  
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcexeService.Models
{
    /// <summary>
    /// 通用消息 Data Transfer Object
    /// </summary>
    public class MessageDto
    {
        public bool Flag { get; set; }

        public string Msg { get; set; }

        public object Data { get; set; }
    }
}
